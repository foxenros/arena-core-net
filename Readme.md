﻿# Arena Core Net
Librería para desarrollo de aplicaciones SPA y MVC con DotNet Core 2.2-

| Project | NuGet |
| ------- | ----- |
| Arena.Core | [![NuGet Badge](https://buildstats.info/nuget/Arena.Core)](https://www.nuget.org/packages/Arena.Core) |

## Características principales

* API REST para comunicación con el cliente.
* Servicios "Trabajar con" con tareas comunes para entidades simples y complejas con asociaciones (cabeceras/detalle).
* Soporte para validaciones campo a campo y de entidad.
* Filtros dinámicos por campos, mediante operadores, sobre las entidades en el servidor.
* Control de concurrencia.
* Mínimos pasos para publicar un servicio de entidad.
* Seguridad:
  - Mediante Middleware utilizando tokens de seguridad.
  - Autenticación incorporada.
  - Autorización por URL. 
  - Perfiles de seguridad para el cliente y el servidor.
  
## Escrita por:
[Hernán Lorenzini](https://www.linkedin.com/in/hlorenzini/)

## Contacto:
herlorenzini@hotmail.com