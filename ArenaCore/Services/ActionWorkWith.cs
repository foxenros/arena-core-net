﻿namespace ArenaSoftware.ArenaCore.Services
{
    /// <summary>
    /// Clase para el enumerador <c>ActionWorkWithEnum</c>. 
    /// </summary>
    public class ActionWorkWith
    {
        /// <value>Constante de acceso para listas.</value>
        public const string ListAccessCode = "@LIST@";

        /// <value>Constante de acceso de consulta.</value>
        public const string GetAccessCode = "V";

        /// <value>Constante de acceso de actualización.</value>
        public const string UpdateAccessCode = "U";

        /// <value>Constante de acceso de inserción.</value>
        public const string InsertAccessCode = "I";

        /// <value>Constante de acceso de eliminación.</value>
        public const string DeleteAccessCode = "D";

        /// <summary>
        /// Enum para acciones de WorkWith. 
        /// </summary>
        public enum ActionWorkWithEnum
        {
            /// <value>Consultar.</value>
            GET = 0,
            /// <value>Actualizar.</value>
            UPDATE = 1,
            /// <value>Insertar.</value>
            INSERT = 2,
            /// <value>Eliminar.</value>
            DELETE = 3,
            /// <value>Cambiar estado.</value>
            CHANGESTATUS = 4
        }
    }

}
