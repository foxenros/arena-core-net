﻿using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaSoftware.ArenaCore.Services
{
    /// <summary>
    /// Clase envolvente de parámetros que se utilizan para enviar información para guardado desde la API Web. 
    /// </summary>
    /// <typeparam name="T">Tipo de los ítems.</typeparam>
    public class SaveParams<T>
    {
        /// <value>Ítem enviado.</value>
        public T Item { set; get; }

        /// <value>Acción ActionWorkWithEnum.</value>
        public ActionWorkWithEnum Action { set; get; }

        /// <summary>
        /// Constructor. 
        /// </summary>
        public SaveParams()
        {
            this.Action = ActionWorkWithEnum.UPDATE;
        }

        /// <summary>
        /// ToString para logging. 
        /// </summary>
        public override string ToString()
        {
            return "SaveParams=[" +
                "Item=" + Item +
                "Action=" + Action + "]";
        }
    }
}
