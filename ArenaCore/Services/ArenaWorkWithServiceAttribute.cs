﻿using System;

namespace ArenaSoftware.ArenaCore.Service
{
    /// <summary>
    /// Clase de atributos con información para servicios WorkWith (trabajar con). 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ArenaWorkWithServiceAttribute : Attribute
    {
        /// <summary>
        /// Constructor con listas de campos. 
        /// </summary>
        /// <param name="keyFields">Lista de campos clave.</param>
        public ArenaWorkWithServiceAttribute(string[] keyFields)
        {
            this.KeyFields = keyFields;
        }

        /// <summary>
        /// Constructor con listas de campos y nodos. 
        /// </summary>
        /// <param name="keyFields">Lista de campos clave.</param>
        /// <param name="eagerNodes">Lista de nodos.</param>
        public ArenaWorkWithServiceAttribute(string[] keyFields, string[] eagerNodes): this(keyFields)
        {
            this.EagerNodes = eagerNodes;
        }

        /// <summary>
        /// Constructor para sólo un campo. 
        /// </summary>
        /// <param name="keyField">Campo clave.</param>
        public ArenaWorkWithServiceAttribute(string keyField) : this(new string[] { keyField })
        {
        }

        /// <summary>
        /// Constructor para sólo un campo y nodos. 
        /// </summary>
        /// <param name="keyField">Campo clave.</param>
        /// <param name="eagerNodes">Lista de nodos.</param>
        public ArenaWorkWithServiceAttribute(string keyField, string[] eagerNodes) : this(new string[] { keyField }, eagerNodes)
        {
        }

        /// <value>Lista de campos clave.</value>
        public string[] KeyFields { get; set; }

        /// <value>Lista de nodos.</value>
        public string[] EagerNodes { get; set; }
    }
}
