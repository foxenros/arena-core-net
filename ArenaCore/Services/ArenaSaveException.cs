﻿using ArenaSoftware.ArenaCore.Commons;
using System;

namespace ArenaSoftware.ArenaCore.Services
{
    /// <summary>
    /// Clase <c>ArenaException</c> sobre el guardado de ítems. 
    /// </summary>
    public class ArenaSaveException : ArenaException
    {
        /// <summary>
        /// Constructor sin parámetros. 
        /// </summary>
        public ArenaSaveException()
        {
        }

        /// <summary>
        /// Constructor con mensaje. 
        /// </summary>
        /// <param name="message">Mensaje de error.</param>
        public ArenaSaveException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructor con mensaje y <c>Exception</c>. 
        /// </summary>
        /// <param name="message">Mensaje de error.</param>
        /// <param name="inner"><c>Exception</c> que disparó.</param>
        public ArenaSaveException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
