﻿using ArenaSoftware.ArenaCore.DataAccess;
using ArenaSoftware.ArenaCore.Filter;
using ArenaSoftware.ArenaCore.Logs;
using ArenaSoftware.ArenaCore.Security;
using ArenaSoftware.ArenaCore.Validation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaSoftware.ArenaCore.Service
{
    /// <summary>
    /// Clase genérica de servicios WorkWith (trabajar con). 
    /// Implementa tareas comunes como "GetItem", "GetItemEager", "GetList", "Validate", "Save", etc.
    /// </summary>
    /// <typeparam name="E">Tipo de los ítems del servicio.</typeparam>
    /// <typeparam name="K">Tipo para el ID. Se debe indicar sólo para clave simple. En caso de claves compuestas indicar <c>object</c></typeparam>
    public class ArenaWorkWithService<E,K> where E : class
    {
        /// <value>Contexto <c>DbContext</c>.</value>
        protected DbContext Context { get; set; }

        /// <value>Útil de validación <c>ArenaValidationUtil</c>.</value>
        protected ArenaValidationUtil ArenaValidationUtil { get; set; }

        /// <value>Generador de filtros<c>FilterBuilder</c>.</value>
        protected FilterBuilder FilterBuilder { get; set; }

        /// <value>Logger <c>ILogger</c>.</value>
        protected ILogger Logger;

        /// <value>Campos clave.</value>
        protected string[] keyFields;

        /// <value>Nodos incluidos.</value>
        protected string[] eagerNodes;

        private string keyExp;

        /// <summary>
        /// Constructor sin parámetros. 
        /// </summary>
        /// 
        public ArenaWorkWithService()
        {
        }

        /// <summary>
        /// Constructor llamado desde las subclases. 
        /// </summary>
        /// <param name="context">DbContext.</param>
        /// <param name="arenaValidationUtil">Útil de validación.</param>
        /// <param name="filterBuilder">Generador de filtros.</param>
        /// <param name="logger">Logger.</param>
        public ArenaWorkWithService(DbContext context, ArenaValidationUtil arenaValidationUtil, FilterBuilder filterBuilder, ILogger logger)
        {
            Init(context, arenaValidationUtil, filterBuilder, logger);
        }

        /// <summary>
        /// Iniciador que debe ser llamado únicamente cuando NO se crea la instancia desde el contructor con parámetros.
        /// </summary>
        /// <param name="context">DbContext.</param>
        /// <param name="arenaValidationUtil">Útil de validación.</param>
        /// <param name="filterBuilder">Generador de filtros.</param>
        /// <param name="logger">Logger.</param>
        public void Init(DbContext context, ArenaValidationUtil arenaValidationUtil, FilterBuilder filterBuilder, ILogger logger) {
            this.Context = context;
            this.ArenaValidationUtil = arenaValidationUtil;
            this.FilterBuilder = filterBuilder;
            this.Logger = logger;
            Init();
        }

        /// <summary>
        /// Iniciador que debe ser llamado únicamente cuando NO se crea la instancia desde el contructor con parámetros.
        /// </summary>
        public void Init()
        {
            SetFromAttributes();
        }

        private void SetFromAttributes()
        {
            var attribute = this.GetType().GetCustomAttributes(
            typeof(ArenaWorkWithServiceAttribute), true).FirstOrDefault() as ArenaWorkWithServiceAttribute;
            if (attribute != null)
            {
                // Toma los atributos.
                this.keyFields = attribute.KeyFields;
                this.eagerNodes = attribute.EagerNodes;

                // Arma la expresión de la clave.
                int n = 0;
                StringBuilder keys = new StringBuilder();
                foreach (string key in this.keyFields)
                {
                    if(n > 0)
                    {
                        keys.Append(" && ");
                    }
                    keys.Append(key + " == @" + n);
                    n++;
                }
                keyExp = keys.ToString();
            }
        }

        /// <summary>
        /// Obtiene un ítem desde el repositorio, incluyendo los nodos declarados como "eagerNodes" en la configuración 
        /// de la clase.
        /// </summary>
        /// <param name="value">ID del ítem.</param>
        /// <returns>
        /// Ítem del repositorio.
        /// </returns>
        public virtual E GetItemEager(K value)
        {
            return this.GetItemEager(new object[] { value });
        }

        /// <summary>
        /// Obtiene un ítem desde el repositorio, incluyendo los nodos declarados como "eagerNodes" en la configuración 
        /// de la clase.
        /// </summary>
        /// <param name="keyValues">Lista de valores ID del ítem.</param>
        /// <returns>
        /// Ítem del repositorio.
        /// </returns>
        public virtual E GetItemEager(object[] keyValues)
        {
            return this.GetItemEager(keyValues, null);
        }

        /// <summary>
        /// Obtiene un ítem desde el repositorio, incluyendo los nodos indicados en el parámetro "eagerNodes".
        /// </summary>
        /// <param name="value">ID del ítem.</param>
        /// <param name="eagerNodes">Lista de nodos "eager" para incluir.</param>
        /// <returns>
        /// Ítem del repositorio.
        /// </returns>
        public virtual E GetItemEager(K value, string[] eagerNodes)
        {
            return this.GetItemEager(new object[] { value }, eagerNodes);
        }

        /// <summary>
        /// Obtiene un ítem desde el repositorio, incluyendo los nodos indicados en el parámetro "eagerNodes".
        /// </summary>
        /// <param name="keyValues">Lista de valores ID del ítem.</param>
        /// <param name="eagerNodes">Lista de nodos "eager" para incluir.</param>
        /// <returns>
        /// Ítem del repositorio.
        /// </returns>
        public virtual E GetItemEager(object[] keyValues, string[] eagerNodes)
        {
            if (eagerNodes == null)
            {
                eagerNodes = this.eagerNodes;
            }

            var query = this.Context.Set<E>().AsQueryable();
            if (eagerNodes != null)
            {
                foreach (string include in eagerNodes)
                {
                    query = query.Include(include);
                }
            }

            this.Logger.LogDebug(LoggingEvents.WorkWithService, "Búsqueda de: keyExp=[{0}], keyValues=[{1}]", keyExp, keyValues);

            E item = null;
            try
            {
                item = query
                    .AsNoTracking()
                    .Where(keyExp, keyValues)
                    .Single();
            }
            catch (System.InvalidOperationException e)
            {
                // No se encontró el elemento, lo deja en null en lugar de fallar.
                // Message = "Sequence contains no elements"
                this.Logger.LogDebug(e.ToString());
            }

            if (item != null)
            {
                this.Logger.LogDebug("Ítem obtenido: {0}", item);
                AfterLoadItemEager(item);
            }

            return item;
        }

        /// <summary>
        /// Callback ejecutado luego de cargar un ítem desde algún método <c>GetItemEager</c>.
        /// </summary>
        protected virtual void AfterLoadItemEager(E item)
        {
            // Nada por hacer.
        }

        /// <summary>
        /// Obtiene un ítem desde el repositorio. 
        /// </summary>
        /// <param name="value">ID del ítem.</param>
        /// <returns>
        /// Ítem del repositorio.
        /// </returns>
        public virtual E GetItem(K value)
        {
            return this.GetItem(new object[] { value });
        }

        /// <summary>
        /// Obtiene un ítem desde el repositorio. 
        /// </summary>
        /// <param name="keyValues">Lista de valores ID del ítem.</param>
        /// <returns>
        /// Ítem del repositorio.
        /// </returns>
        public virtual E GetItem(params object[] keyValues)
        {
            this.Logger.LogDebug(LoggingEvents.WorkWithService, "Búsqueda de: keyExp=[{0}], keyValues=[{1}]", keyExp, keyValues);

            var query = this.Context.Set<E>().AsQueryable();

            E item = null;
            try
            {
                item = query
                    .AsNoTracking()
                    .Where(keyExp, keyValues)
                    .FirstOrDefault();
            }
            catch (System.InvalidOperationException e)
            {
                // No se encontró el elemento, lo deja en null en lugar de fallar.
                // Message = "Sequence contains no elements"
                this.Logger.LogDebug(e.ToString());
            }


            if (item != null)
            {
                this.Logger.LogDebug(LoggingEvents.WorkWithService, "Ítem obtenido: {0}", item);
            }

            return item;
        }

        /// <summary>
        /// Obtiene una lista de ítems de acuerdo al filtro <c>ArenaFilter</c> indicado. 
        /// </summary>
        /// <param name="arenaFilter">Filtro para aplicar.</param>
        /// <returns>
        /// Lista de ítems del repositorio.
        /// </returns>
        public virtual List<E> GetList(ArenaFilter arenaFilter)
        {
            var query = this.Context.Set<E>().AsQueryable();
            // Agrega los nodos necesarios (FKs).
            foreach (string include in arenaFilter.ExtraNodes)
            {
                query = query.Include(include);
            }

            List<WhereParameter> properties = this.FilterBuilder.Build(arenaFilter);
            QueryWhere queryWhere = this.FilterBuilder.QueryWhereBuilder.Build(properties);

            var values = queryWhere.Parameters.ToDynamicArray();
            this.Logger.LogDebug(LoggingEvents.WorkWithService, "Búsqueda de lista: Where=[{0}], values=[{1}]", queryWhere.Where, values);

            var list = query
                .AsNoTracking()
                // Agrega el where dinámico.
                .Where(queryWhere.Where, values)
                .ToList();

            this.Logger.LogDebug(LoggingEvents.WorkWithService, "Se obtuvieron {0} registros", list.Count);

            return list;
        }

        /// <summary>
        /// Ejecuta las acciones de validación de la clase para la acción <c>ActionWorkWithEnum</c> indicada.
        /// </summary>
        /// <param name="item">Ítem a guardar.</param>
        /// <param name="action">Acción <c>ActionWorkWithEnum</c></param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        public virtual ArenaValidationResponse<E> Validate(E item, ActionWorkWithEnum action)
        {
            this.Logger.LogDebug(LoggingEvents.WorkWithService, "Se valida desde el servicio: item={0}, action={1}", item, action);
            var avr = this.ArenaValidationUtil.TryValidateObject<E>(item, action);
            return avr;
        }

        /// <summary>
        /// Ejecuta una validación específica de acuerdo a los parámetros indicados en <c>ValidationInfo</c>.
        /// </summary>
        /// <param name="validationInfo">Información para realizar la validación.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        public virtual ArenaValidationResponse Validate(ValidationInfo validationInfo)
        {
            var avr = this.ArenaValidationUtil.Validate(validationInfo);
            return avr;
        }

        /// <summary>
        /// Callback llamado antes de guardar para limpiar objetos FK del ítem que puedan causar posibles problemas por estar fuera del ámbito de EF.
        /// </summary>
        /// <remarks>
        /// Este problema se produce cuando los objetos son conformados desde llamadas de API Web, de esa manera NO se garantiza que se encuentren las referencias en el contexto <c>DbContext</c>.
        /// EF puede intentar agregar los objetos FK en la base de datos al guardar y esto NO es deseable ya que cuasaría errores de FK de clave duplicada.
        /// En este método se debe programar la limpieza necesaria pasando los objetos FK a null y así dejar sólo las claves con datos para el repositorio o la base de datos.
        /// </remarks>
        protected virtual void Clean(E item)
        {
            // Nada por hacer.
        }

        /// <summary>
        /// Callback llamado antes de guardar para permitir programar tareas necesarias y específicas.
        /// </summary>
        protected virtual void PreSave(E item, ActionWorkWithEnum action)
        {
            // Nada por hacer.
        }

        /// <summary>
        /// Guarda el ítem en el almacenamiento.
        /// </summary>
        /// <param name="item">Ítem a guardar.</param>
        /// <param name="action">Acción del evento.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        public virtual ArenaValidationResponse<E> Save(E item, ActionWorkWithEnum action)
        {
            PreSave(item, action);

            var avr = this.Validate(item, action);

            if (!avr.HasErrors())
            {
                this.Clean(item);

                try
                {
                    using (var transaction = this.Context.Database.BeginTransaction())
                    {
                        if (action == ActionWorkWithEnum.INSERT)
                        {
                            this.Context.Add(item);
                        }
                        else
                        {
                            List<object> keyValues = new List<object>();
                            foreach (string key in this.keyFields)
                            {
                                object value = item.GetType().GetProperty(key).GetValue(item);
                                keyValues.Add(value);
                            }

                            this.Detach(item, keyValues.ToArray());

                            if (action == ActionWorkWithEnum.DELETE)
                            {
                                this.Context.Remove(item);
                            }
                            else
                            {
                                this.Context.Update(item);
                            }
                        }
                        this.Context.SaveChanges();

                        transaction.Commit();
                    }
                }
                catch (DbUpdateConcurrencyException e)
                {
                    ArenaError ae = new ArenaError();
                    ae.MsgErrorDescription = "El objeto fue guardado por otro proceso o usuario.";
                    ae.MsgErrorKey = "ConcurrencyError";
                    avr.Errors.Add(ae);
                    this.Logger.LogDebug(LoggingEvents.WorkWithService, "El objeto fue guardado por otro proceso o usuario: ArenaError={0}, {1}", ae, e);
                }
                catch (Exception e)
                {
                    ArenaError ae = new ArenaError();
                    ae.MsgErrorDescription = "No se pudo actualizar. " + e.Message;
                    ae.MsgErrorKey = "cantSave";
                    avr.Errors.Add(ae);
                    this.Logger.LogDebug(LoggingEvents.WorkWithService, "No se pudo actualizar: ArenaError={0}", ae);
                }
            }
            return avr;
        }


        /// <summary>
        /// Callback llamado antes de guardar para limpiar el contexto y agregar al ítem como modificado (EntityState.Modified).
        /// </summary>
        protected virtual void Detach(E item, object[] keyValues)
        {
            var local = this.Context.Set<E>()
                .Local
                .AsQueryable()
                .Where(keyExp, keyValues)
                .FirstOrDefault();

            if (local != null)
            {
                this.Context.Entry(local).State = EntityState.Detached;
            }
            this.Context.Entry(item).State = EntityState.Modified;
        }
    }
}
