﻿using System.Collections.Generic;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Interfaz para perfil de seguridad. 
    /// </summary>
    public interface IProfileUtil
    {
        /// <value>Indica si el usuario es administrador de seguridad.</value>
        bool IsAdmin(string userName);

        /// <value>Diccionario con el perfil de seguridad.</value>
        Dictionary<string, ProfileItem> GetProfile(string userName);
    }
}
