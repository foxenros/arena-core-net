﻿namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Interfaz para token de seguridad. 
    /// </summary>
    public interface ITokenBuilder
    {

        /// <value>Minutos para expiración de token.</value>
        int TokenMinutesExpire { set; get; }

        /// <summary>
        /// Crea el token de seguridad. 
        /// </summary>
        /// <param name="basicAuthenticationIdentity">Información de autenticación básica.</param>
        /// <returns>
        /// El token creado.
        /// </returns>
        string CreateToken(BasicAuthenticationIdentity basicAuthenticationIdentity);

        /// <summary>
        /// Crea un hash en base a la entrada. 
        /// </summary>
        /// <param name="input">Cadena de entrada.</param>
        /// <returns>
        /// Un hash de la cadena.
        /// </returns>
        string HashMD5(string input);

        /// <summary>
        /// Devuelve el nombre de usuario desde el token. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <returns>
        /// El usuario desde el token.
        /// </returns>
        string UsernameFromToken(string authToken);

        /// <summary>
        /// Devuelve la empresa desde el token. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <returns>
        /// La empresa desde el token.
        /// </returns>
        string CompanyFromToken(string authToken);

        /// <summary>
        /// Devuelve el origen desde el token. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <returns>
        /// El origen desde el token.
        /// </returns>
        string OriginFromToken(string authToken);

        /// <summary>
        /// Devuelve si el token es válido. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <param name="basicAuthenticationIdentity">información de autenticación básica.</param>
        /// <returns>
        /// Si es válido el token.
        /// </returns>
        bool ValidateToken(string authToken, BasicAuthenticationIdentity basicAuthenticationIdentity);
    }
}
