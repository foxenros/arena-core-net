﻿namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Interfaz de usario para repositorio de usuarios. 
    /// </summary>
    public interface IUser
    {
        /// <value>Nombre del usuario.</value>
        string Username { get; set; }

        /// <value>Contraseña.</value>
        string Password { get; set; }

        /// <value>Inactivo.</value>
        bool Inactive { get; set; }
    }
}
