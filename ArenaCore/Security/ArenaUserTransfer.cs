﻿using System.Collections.Generic;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Clase con información del usuario autenticado. 
    /// </summary>
    public class ArenaUserTransfer
    {
        /// <value>Nombre del usuario.</value>
        public string Username { get; set; }

        /// <value>indica si es administrador de seguridad.</value>
        public bool IsAdmin { get; set; }

        /// <value>Diccionario con el perfil de seguridad.</value>
        public Dictionary<string, ProfileItem> ProfileItems { get; set; }
    }
}
