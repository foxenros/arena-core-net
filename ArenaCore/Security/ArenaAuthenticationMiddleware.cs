﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Middleware para autenticación de seguridad (singletón). 
    /// </summary>
    /// <remarks>
    /// En caso de usar este Middleware estilo convencional (en lugar de uno IMiddleware): se debe implementar IUserRepository como singletón. 
    /// Esto NO permitirá utilizar DI para el DbContext u otro servicio scoped. Para resolverlo se debe utilizar IServiceScopeFactory para crear 
    /// el DbContext en la implementación de IUserRepository.
    /// </remarks>
    /// <example>
    /// <code>
    /// public class AppUserRepository : IUserRepository
    /// {
    ///     private readonly IServiceScopeFactory _scopeFactory;
    /// 
    ///     public AppUserRepository(IServiceScopeFactory scopeFactory)
    ///     {
    ///         _scopeFactory = scopeFactory;
    /// 
    ///     }
    ///     public IUser GetItem(string username)
    ///     {
    ///         using (var scope = _scopeFactory.CreateScope())
    ///         {
    ///             var context = scope.ServiceProvider.GetService/<ArenaSecurityDbContext/>();
    ///             User item = context.Users
    ///                 .Where(b => b.Username == username)
    ///                 .FirstOrDefault();
    ///             return item;
    ///         }
    ///     }
    /// }    
    /// </code>
    /// </example>
    public class ArenaAuthorizationMiddleware : ArenaAuthentication
    {
        private readonly RequestDelegate _next;
        private readonly IUserRepository _userRepository;
        private readonly ITokenBuilder _tokenBuilder;
        private readonly string[] urlExceptions;
        private readonly string[] urlSecure;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor con RequestDelegate, repositorio de usuarios, generador de tokens, manejador de configuración y logger. 
        /// </summary>
        /// <param name="next">El delegado que representa el middleware restante en la canalización de solicitud.</param>
        /// <param name="userRepository">Repositorio de usuarios.</param>
        /// <param name="tokenBuilder">Generador de tokens.</param>
        /// <param name="configuration">Manejador de configuración.</param>
        /// <param name="logger">Logger.</param>
        public ArenaAuthorizationMiddleware(RequestDelegate next, 
            IUserRepository userRepository, 
            ITokenBuilder tokenBuilder, 
            IConfiguration configuration,
            ILogger<ArenaAuthorizationMiddleware> logger)
        {
            _next = next;
            _userRepository = userRepository;
            _tokenBuilder = tokenBuilder;
            string strUrl = configuration.GetSection("ArenaSecurity").GetValue<string>("AuthorizationFilterUrlExceptions");
            if (!string.IsNullOrEmpty(strUrl))
            {
                urlExceptions = strUrl.Split(",");
            }
            string strSecureUrl = configuration.GetSection("ArenaSecurity").GetValue<string>("AuthorizationFilterUrl");
            if (!string.IsNullOrEmpty(strSecureUrl))
            {
                urlSecure = strSecureUrl.Split(",");
            }

            _logger = logger;
        }

        /// <summary>
        /// Método de manejo de solicitudes.
        /// </summary>
        /// <param name="context">El Microsoft.AspNetCore.Http.HttpContext para la solicitud actual.</param>
        public async Task Invoke(HttpContext context)
        {
            if (await base.Authenticate(context, _userRepository, _tokenBuilder, urlExceptions, _logger, urlSecure))
            {
                await _next(context);
            }

        }

    }

    #region ExtensionMethod
    /// <summary>
    /// Extensión para IApplicationBuilder "ArenaAuthorizationExtension". 
    /// </summary>
    public static class ArenaAuthorizationExtension
    {
        /// <summary>
        /// Habilita uso UseArenaAuthorization. 
        /// </summary>
        /// <param name="app"><c>IApplicationBuilder</c></param>
        public static IApplicationBuilder UseArenaAuthorization(this IApplicationBuilder app)
        {
            app.UseMiddleware<ArenaAuthorizationMiddleware>( );
            return app;
        }
    }
    #endregion

}
