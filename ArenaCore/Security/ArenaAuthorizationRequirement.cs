﻿using Microsoft.AspNetCore.Authorization;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Requerimiento de seguridad (estándar Arena) para los permisos de menú. 
    /// </summary>
    public class ArenaAuthorizationRequirement : IAuthorizationRequirement
    {
        /// <value>Nombre de la política.</value>
        public const string PolicyName = "ArenaAuthorization";
    }
}
