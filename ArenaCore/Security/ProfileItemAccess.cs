﻿namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Clase con información de acceso de seguridad de ítems de menú. 
    /// </summary>
    public class ProfileItemAccess
    {
        /// <summary>
        /// Constructor con código de acceso y indicador de permiso. 
        /// </summary>
        /// <param name="accessCode">Código de acceso.</param>
        /// <param name="enabled">Indicador de permiso.</param>
        public ProfileItemAccess(string accessCode, bool enabled)
        {
            this.AccessCode = accessCode;
            this.Enabled = enabled;
        }

        /// <value>Código de acceso.</value>
        public string AccessCode { get; set; }

        /// <value>Descripción del acceso.</value>
        public string AccessDesc { get; set; }

        /// <value>Indica si es permitido el ingreso al acceso.</value>
        public bool Enabled { get; set; }
    }
}
