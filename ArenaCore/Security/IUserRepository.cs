﻿using ArenaSoftware.ArenaCore.Security;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Interfaz de repositorio de usuarios. 
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Devuelve un usuario <c>IUser</c> desde el repositorio de usuarios. 
        /// </summary>
        /// <param name="username">Nombre de usuario.</param>
        /// <returns>
        /// El usuario <c>IUser</c>.
        /// </returns>
        IUser GetItem(string username);
    }
}
