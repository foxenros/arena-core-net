﻿namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Clase con información de autenticación. 
    /// </summary>
    public class AuthRequest
    {
        /// <value>Nombre del usuario.</value>
        public string Username { get; set; }

        /// <value>Contraseña.</value>
        public string Password { get; set; }

        /// <value>Empresa.</value>
        public string Company { get; set; }

        /// <value>Origen.</value>
        public string Origin { get; set; }
    }
}
