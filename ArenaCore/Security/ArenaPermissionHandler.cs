﻿using ArenaSoftware.ArenaCore.Controllers;
using ArenaSoftware.ArenaCore.Logs;
using ArenaSoftware.ArenaCore.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Manejador de permisos (estándar Arena) para los permisos de menú y acciones de seguridad. 
    /// </summary>
    public class ArenaPermissionHandler : AuthorizationHandler<ArenaAuthorizationRequirement>
    {
        private readonly IPermissionChecker _permissionChecker;

        /// <value>Logger.</value>
        protected ILogger Logger;

        /// <summary>
        /// Constructor con manejador de permisos y logger. 
        /// </summary>
        /// <param name="permissionChecker">Manejador de permisos.</param>
        /// <param name="logger">Logger.</param>
        public ArenaPermissionHandler(IPermissionChecker permissionChecker, ILogger<ArenaPermissionHandler> logger)
        {
            _permissionChecker = permissionChecker;
            this.Logger = logger;
        }

        /// <summary>
        /// Toma una decisión de autorización según un requisito específico.
        /// </summary>
        /// <param name="context">Contexto de seguridad.</param>
        /// <param name="requirement">Requerimiento a evaluar.</param>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ArenaAuthorizationRequirement requirement)
        {
            try
            {
                if (context.Resource is AuthorizationFilterContext mvcContext)
                {
                    //string userName = context.User.FindFirst("userName").Value;
                    GenericPrincipal principal = (GenericPrincipal)Thread.CurrentPrincipal;
                    string userName = principal.Identity.Name;

                    ActionDescriptor ad = mvcContext.ActionDescriptor;
                    Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor cad = (Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)ad;
                    TypeInfo typeInfo = cad.ControllerTypeInfo;

                    // Lee los atributos de ArenaAuthorizationAttribute.
                    var arenaAuthorization = typeInfo.AsType().GetCustomAttributes(typeof(ArenaAuthorizationAttribute), true)[0] as ArenaAuthorizationAttribute;

                    if (_permissionChecker.CheckPermission(userName, arenaAuthorization.MenuItId))
                    {
                        // Lee los atributos de ArenaAuthorizationActionAttribute.
                        var arenaAuthorizationAction = cad.MethodInfo.GetCustomAttribute(typeof(ArenaAuthorizationActionAttribute), true) as ArenaAuthorizationActionAttribute;
                        if (arenaAuthorizationAction == null)
                        {
                            // NO Hay directiva arenaAuthorizationAction, entonces con la autorización para el MenuItId basta.
                            context.Succeed(requirement);
                        }
                        else
                        {
                            // Se revisa el acceso de seguridad solicitado.
                            string accessCode = arenaAuthorizationAction.AccessCode;
                            if (accessCode == ActionWorkWith.ListAccessCode)
                            {
                                if (_permissionChecker.CheckListPermission(userName, arenaAuthorization.MenuItId))
                                {
                                    // Tiene el permiso para listas.
                                    context.Succeed(requirement);
                                }
                            }
                            else
                            {
                                if (_permissionChecker.CheckPermission(userName, arenaAuthorization.MenuItId, arenaAuthorizationAction.AccessCode))
                                {
                                    // Tiene el permiso para el acceso requerido.
                                    context.Succeed(requirement);
                                }
                            }
                        }

                        if (!context.HasSucceeded) {
                            // No hay autorización, se muestra el log.
                            this.Logger.LogDebug(LoggingEvents.Security, "No autorizado");
                        }

                    }
                }

            } catch(Exception e)
            {
                // No hay autorización, se muestra el log.
                this.Logger.LogDebug(LoggingEvents.Security, "No autorizado: " + e.Message);
            }
            return Task.CompletedTask;
        }
    }
}
