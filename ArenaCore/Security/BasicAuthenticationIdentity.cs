﻿using System.Security.Principal;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Implementación de autenticación básica (Arena). 
    /// </summary>
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        /// <value>Nombre del usuario.</value>
        public string Username { get; set; }
        
        /// <value>Contraseña.</value>
        public string Password { get; set; }

        /// <value>Empresa.</value>
        public string Company { get; set; }

        /// <value>Origen.</value>
        public string Origin { get; set; }

        /// <value>Id del usuario.</value>
        public int UserId { get; set; }

        /// <summary>
        /// Constructor con usuario, contraseña, empresa y origen. 
        /// </summary>
        /// <param name="username">Usuario.</param>
        /// <param name="password">Contraseña.</param>
        /// <param name="company">Empresa.</param>
        /// <param name="origin">Origen.</param>
        public BasicAuthenticationIdentity(string username, string password, string company, string origin)
            : base(username, "Basic")
        {
            this.Password = password;
            this.Username = username;
            this.Company = company;
            this.Origin = origin;
        }
    }
}
