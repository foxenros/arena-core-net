﻿using System;
using System.Text;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ArenaSoftware.ArenaCore.Logs;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Implementación (estándar Arena) <c>ITokenBuilder</c> para token de seguridad. 
    /// </summary>
    public class TokenBuilder : ITokenBuilder
    {
        private static int FIELD_USERNAME = 0;
        private static int FIELD_EXPIRE = 1;
        private static int FIELD_COMPANY = 2;
        private static int FIELD_SIGNATURE = 3;
        private static int FIELD_ORIGIN = 4;

        private readonly string _tokenMagicKey;
        
        /// <value>Minutos para expiración de token.</value>
        public int TokenMinutesExpire { set; get; }

        /// <value>Logger.</value>
        protected ILogger Logger;

        /// <summary>
        /// Constructor con manejador de seguridad y logger. 
        /// </summary>
        /// <param name="configuration">Manejador de seguridad.</param>
        /// <param name="logger">Logger.</param>
        public TokenBuilder(IConfiguration configuration, ILogger<TokenBuilder> logger)
        {
            _tokenMagicKey = configuration.GetSection("ArenaSecurity").GetValue<string>("TokenMagicKey");
            this.TokenMinutesExpire = configuration.GetSection("ArenaSecurity").GetValue<int>("TokenMinutesExpire");
            this.Logger = logger;
        }

        /// <summary>
        /// Crea el token de seguridad. 
        /// </summary>
        /// <param name="basicAuthenticationIdentity">Información de autenticación básica.</param>
        /// <returns>
        /// El token creado.
        /// </returns>
        public string CreateToken(BasicAuthenticationIdentity basicAuthenticationIdentity)
        {
            long expires = DateTimeOffset.Now.ToUnixTimeMilliseconds() + 1000L * 60 * this.TokenMinutesExpire; ;

            StringBuilder tokenBuilder = new StringBuilder();
            tokenBuilder.Append(basicAuthenticationIdentity.Username);
            tokenBuilder.Append(":");
            tokenBuilder.Append(expires);
            tokenBuilder.Append(":");
            tokenBuilder.Append(basicAuthenticationIdentity.Company);
            tokenBuilder.Append(":");
            tokenBuilder.Append(ComputeSignature(basicAuthenticationIdentity, expires));
            tokenBuilder.Append(":");
            tokenBuilder.Append(basicAuthenticationIdentity.Origin);

            return tokenBuilder.ToString();
        }

        private string ComputeSignature(BasicAuthenticationIdentity basicAuthenticationIdentity, long expires)
        {
            StringBuilder signatureBuilder = new StringBuilder();
            // user:expires:password:tokenMagicKey
            signatureBuilder.Append(basicAuthenticationIdentity.Username);
            signatureBuilder.Append(":");
            signatureBuilder.Append(expires);
            signatureBuilder.Append(":");
            signatureBuilder.Append(basicAuthenticationIdentity.Password);
            signatureBuilder.Append(":");
            signatureBuilder.Append(_tokenMagicKey);

            return HashMD5(signatureBuilder.ToString());
        }

        /// <summary>
        /// Crea un hash en base a la entrada. 
        /// </summary>
        /// <param name="input">Cadena de entrada.</param>
        /// <returns>
        /// Un hash de la cadena.
        /// </returns>
        public string HashMD5(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Devuelve el nombre de usuario desde el token. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <returns>
        /// El usuario desde el token.
        /// </returns>
        public string UsernameFromToken(string authToken)
        {
            if (authToken == null)
            {
                return null;
            }
            String[] parts = authToken.Split(":");
            return parts[FIELD_USERNAME];
        }

        /// <summary>
        /// Devuelve la empresa desde el token. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <returns>
        /// La empresa desde el token.
        /// </returns>
        public string CompanyFromToken(string authToken)
        {
            if (authToken == null)
            {
                return null;
            }
            String[] parts = authToken.Split(":");
            return parts[FIELD_COMPANY];
        }

        /// <summary>
        /// Devuelve el origen desde el token. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <returns>
        /// El origen desde el token.
        /// </returns>
        public string OriginFromToken(string authToken)
        {
            if (authToken == null)
            {
                return null;
            }
            String[] parts = authToken.Split(":");
            if (parts.Length - 1 >= FIELD_ORIGIN)
            {
                return parts[FIELD_ORIGIN];
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Devuelve si el token es válido. 
        /// </summary>
        /// <param name="authToken">Token.</param>
        /// <param name="basicAuthenticationIdentity">información de autenticación básica.</param>
        /// <returns>
        /// Si es válido el token.
        /// </returns>
        public bool ValidateToken(string authToken, BasicAuthenticationIdentity basicAuthenticationIdentity)
        {
            string[] parts = authToken.Split(":");
            long expires = Int64.Parse(parts[FIELD_EXPIRE]);
            string signature = parts[FIELD_SIGNATURE];

            long nowMilis = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            if (expires < nowMilis)
            {
                this.Logger.LogDebug(LoggingEvents.Security, "Token expirado={0}", authToken);
                return false;
            }

            string compSignature = ComputeSignature(basicAuthenticationIdentity, expires);
            return signature.Equals(compSignature);
        }

    }
}
