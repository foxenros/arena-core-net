﻿using ArenaSoftware.ArenaCore.Logs;
using ArenaSoftware.ArenaCore.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Implementación (estándar Arena) de <c>IPermissionChecker</c> para autorización de permisos. 
    /// </summary>
    public class PermissionChecker : IPermissionChecker
    {
        private static Dictionary<string, UserAccess> userAccessMap;

        private short profileCacheMinutesExpire = 0;
        private readonly IProfileUtil _profileUtil;

        /// <value>Logger.</value>
        protected ILogger Logger;

        static PermissionChecker(){
            userAccessMap = new Dictionary<string, UserAccess>();
        }

        /// <summary>
        /// Constructor con útil de perfil de seguridad, manejador de configuración y logger. 
        /// </summary>
        /// <param name="profileUtil">Útil de perfil de seguridad.</param>
        /// <param name="configuration">Manejador de configuración.</param>
        /// <param name="logger">Logger.</param>
        public PermissionChecker(IProfileUtil profileUtil, IConfiguration configuration, ILogger<PermissionChecker> logger)
        {
            this._profileUtil = profileUtil;
            this.profileCacheMinutesExpire = configuration.GetSection("ArenaSecurity").GetValue<short>("ProfileCacheMinutesExpire");
            this.Logger = logger;
        }

        /// <summary>
        /// Devuelve si autoriza la entrada al ítem de menú. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        public bool CheckPermission(string userName, string menuItId)
        {
            UserAccess userAccess = GetProfileCache(userName);
            if (userAccess.IsAdmin)
            {
                return true;
            } else
            {
                Dictionary<string, ProfileItem> profileItems = userAccess.Profile;
                string key = menuItId;
                if (profileItems.ContainsKey(key))
                {
                    ProfileItem pi = profileItems[key];
                    return (pi != null && pi.Enabled);
                }
                return false;
            }
        }

        /// <summary>
        /// Devuelve si autoriza la entrada al ítem de menú en la acción indicada. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <param name="actionCode">Acción <c>ActionWorkWithEnum</c>.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        public bool CheckPermission(string userName, string menuItId, ActionWorkWithEnum actionCode)
        {
            string accessCode = null;
            if (actionCode == ActionWorkWithEnum.INSERT)
            {
                // Acceso a acción de ALTA. 
                accessCode = ActionWorkWith.InsertAccessCode;
            }
            else if (actionCode == ActionWorkWithEnum.UPDATE)
            {
                // Acceso a acción de ACTUALIZACIÓN. 
                accessCode = ActionWorkWith.UpdateAccessCode;
            }
            else if (actionCode == ActionWorkWithEnum.DELETE)
            {
                // Acceso a acción de BAJA. 
                accessCode = ActionWorkWith.DeleteAccessCode;
            }
            else if (actionCode == ActionWorkWithEnum.GET)
            {
                // Acceso a acción de CONSULTA.
                accessCode = ActionWorkWith.GetAccessCode;
            }
            return this.CheckPermission(userName, menuItId, accessCode);
        }

        /// <summary>
        /// Devuelve si autoriza la entrada al ítem de menú y el acceso indicado. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <param name="accessCode">Codigo de acceso.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        public bool CheckPermission(string userName, string menuItId, string accessCode)
        {
            UserAccess userAccess = GetProfileCache(userName);
            if (userAccess.IsAdmin)
            {
                return true;
            }
            else
            {
                Dictionary<string, ProfileItem> profileItems = userAccess.Profile;
                string key = menuItId;
                if (profileItems.ContainsKey(key))
                {
                    ProfileItem pi = profileItems[key];
                    string keyAC = accessCode.ToUpper();
                    if (pi.ProfileItemAccess.ContainsKey(keyAC))
                    {
                        ProfileItemAccess acc = pi.ProfileItemAccess[keyAC];
                        return (acc != null && acc.Enabled);
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Devuelve si autoriza la entrada a la lista asociada al ítem de menú. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        public bool CheckListPermission(string userName, string menuItId)
        {
            // Seguridad - No hay mecanismo definido para permiso de nóminas.
            return true;
        }

        /// <summary>
        /// Limpia el cache. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void ClearUserCache(string userName)
        {
            this.Logger.LogDebug(LoggingEvents.Security, "Se limpia el cache para [{0}]", userName);
            userAccessMap.Remove(userName);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private UserAccess GetProfileCache(string username)
        {
            Dictionary<string, ProfileItem> profile = null;
            bool isAdmin = false;

            // Revisa el caché.
            UserAccess userAccess = null;
            if (userAccessMap.ContainsKey(username))
            {
                userAccess = userAccessMap[username];

                DateTime expire = DateTime.Now;
                expire.AddMinutes(profileCacheMinutesExpire);
                if (userAccess.Time > expire)
                {
                    userAccessMap.Remove(username);
                    userAccess = null;
                } else
                {
                    this.Logger.LogDebug(LoggingEvents.Security, "Se toma perfil de [{0}] del cache", username);
                }
            }
            if (userAccess == null)
            {
                // Toma del repositorio y guarda en caché.
                isAdmin = this._profileUtil.IsAdmin(username);
                if (!isAdmin)
                {
                    profile = this._profileUtil.GetProfile(username);
                } else
                {
                    profile = new Dictionary<string, ProfileItem>();
                }
                userAccess = new UserAccess(profile, isAdmin);
                this.Logger.LogDebug(LoggingEvents.Security, "Se crea perfil de [{0}] en el cache", username);
                userAccessMap.Add(username, userAccess);
            }

            return userAccess;

        }

        internal class UserAccess
        {
            public UserAccess(Dictionary<string, ProfileItem> profile, bool isAdmin)
            {
                this.Profile = profile;
                this.Time = DateTime.Now;
                this.IsAdmin = isAdmin;
            }

            public Dictionary<string, ProfileItem> Profile { set; get; }
            public DateTime Time { set; get; }
            public bool IsAdmin { set; get; }
        }
    }
}
