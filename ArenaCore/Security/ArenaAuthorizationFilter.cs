﻿using ArenaSoftware.ArenaCore.Logs;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Filtro para seguridad. Implementa <c>IAuthorizationFilter</c> para confirmar las solicitudes de autorización. 
    /// </summary>
    /// 
    [Obsolete("Utilzar ArenaAuthenticationIMiddleware o ArenaAuthorizationMiddleware.")]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class ArenaAuthorizationFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        /// <value>Logger.</value>
        protected ILogger Logger;

        private readonly IUserRepository _userRepository;
        private readonly ITokenBuilder _tokenBuilder;
        private readonly string[] urlExceptions;

        /// <summary>
        /// Constructor con logger, repositorio de usuarios, generador de tokens y manejador de configuración. 
        /// </summary>
        /// <param name="logger">Logger.</param>
        /// <param name="userRepository">Repositorio de usuarios.</param>
        /// <param name="tokenBuilder">Generador de tokens.</param>
        /// <param name="configuration">Manejador de configuración.</param>
        public ArenaAuthorizationFilter(ILogger<ArenaAuthorizationFilter> logger, 
            IUserRepository userRepository, 
            ITokenBuilder tokenBuilder, 
            IConfiguration configuration)
        {
            this.Logger = logger;
            _userRepository = userRepository;
            _tokenBuilder = tokenBuilder;
            string strUrl = configuration.GetSection("ArenaSecurity").GetValue<string>("AuthorizationFilterUrlExceptions");
            if (!string.IsNullOrEmpty(strUrl))
            {
                urlExceptions = strUrl.Split(",");
            }
        }

        /// <summary>
        /// Se llama al principio de la tubería del filtros para confirmar que la solicitud esté autorizada.
        /// </summary>
        /// <param name="context"><c>Microsoft.AspNetCore.Mvc.Filters.AuthorizationFilterContext</c></param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string requestUrl = context.HttpContext.Request.Path.Value;
            bool checkUrl = true;
            foreach (string url in urlExceptions)
            {
                if(url == requestUrl)
                {
                    checkUrl = false;
                    break;
                } else if (url.EndsWith("*"))
                {
                    // Revisa urls con el comodín '*'.
                    string endUrl = url.Substring(0, url.Length - 1);
                    if (requestUrl.StartsWith(endUrl))
                    {
                        this.Logger.LogDebug(LoggingEvents.Security, "URL [{0}] exceptuada para autorización", requestUrl);
                        checkUrl = false;
                        break;
                    }
                }
            }

            if (checkUrl)
            {
                string authToken = ExtractAuthTokenFromRequest(context.HttpContext);
                string username = _tokenBuilder.UsernameFromToken(authToken);
                string company = _tokenBuilder.UsernameFromToken(authToken);
                string origin = _tokenBuilder.OriginFromToken(authToken);

                if (!string.IsNullOrEmpty(username))
                {
                    IUser user = _userRepository.GetItem(username);
                    BasicAuthenticationIdentity identity = new BasicAuthenticationIdentity(username, user.Password, company, origin);

                    // ValidateToken valida la contraseña y su expiración. 
                    if (identity != null && _tokenBuilder.ValidateToken(authToken, identity))
                    {
                        var genericPrincipal = new GenericPrincipal(identity, null);
                        Thread.CurrentPrincipal = genericPrincipal;

                        Task taskSignIn = SignIn(context, username);
                        taskSignIn.ConfigureAwait(false);
                        return;
                    }
                }

                Task taskSignOut = SignOut(context);
                taskSignOut.ConfigureAwait(false);

                Thread.CurrentPrincipal = null;
                this.Logger.LogDebug(LoggingEvents.Security, "URL [{0}] no autorizada para {1} ", requestUrl, username);
                context.Result = new Microsoft.AspNetCore.Mvc.UnauthorizedResult();
            }

        }

        private async Task SignIn(AuthorizationFilterContext context, string userName)
        {
            var claims = new List<Claim>
            {
                new Claim("userName", userName)
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);
            
            Task task = AuthenticationHttpContextExtensions.SignInAsync(
                context.HttpContext,
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity));

            await task;
        }

        private async Task SignOut(AuthorizationFilterContext context)
        {
            await AuthenticationHttpContextExtensions.SignOutAsync(context.HttpContext);
        }

        private string ExtractAuthTokenFromRequest(HttpContext httpContext)
        {
            string authToken = httpContext.Request.Headers["X-Auth-Token"];
            return authToken;
        }
    }
}
