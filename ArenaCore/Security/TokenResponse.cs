﻿using System.Net.Http;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Clase con información del token de seguridad. 
    /// </summary>
    public class TokenResponse : HttpResponseMessage
    {
        /// <summary>
        /// Constructor con token. 
        /// </summary>
        /// <param name="token">Token.</param>
        public TokenResponse(string token)
        {
            this.Token = token;
        }

        /// <value>Token.</value>
        public string Token { get; set; }
    }
}