﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Middleware para autenticación de seguridad.
    /// </summary>
    /// <remarks>
    /// Autentica en base a token implementando IMiddleware.
    /// Puede utilzar servicios scoped (se instancia por cada llamada).
    /// </remarks>
    public class ArenaAuthenticationIMiddleware: ArenaAuthentication, IMiddleware
    {
        private readonly IUserRepository _userRepository;
        private readonly ITokenBuilder _tokenBuilder;
        private readonly string[] urlExceptions;
        private readonly string[] urlSecure;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor con repositorio de usuarios (scoped), generador de tokens, manejador de configuración y logger. 
        /// </summary>
        /// <param name="userRepository">Repositorio de usuarios (scoped).</param>
        /// <param name="tokenBuilder">Generador de tokens.</param>
        /// <param name="configuration">Manejador de configuración.</param>
        /// <param name="logger">Logger.</param>
        public ArenaAuthenticationIMiddleware(IUserRepository userRepository, 
            ITokenBuilder tokenBuilder, 
            IConfiguration configuration,
            ILogger<ArenaAuthenticationIMiddleware> logger)
        {
            _userRepository = userRepository;
            _tokenBuilder = tokenBuilder;
            string strUrl = configuration.GetSection("ArenaSecurity").GetValue<string>("AuthorizationFilterUrlExceptions");
            if (!string.IsNullOrEmpty(strUrl))
            {
                urlExceptions = strUrl.Split(",");
            }
            string strSecureUrl = configuration.GetSection("ArenaSecurity").GetValue<string>("AuthorizationFilterUrl");
            if (!string.IsNullOrEmpty(strSecureUrl))
            {
                urlSecure = strSecureUrl.Split(",");
            }
            _logger = logger;
        }

        /// <summary>
        /// Método de manejo de solicitudes.
        /// </summary>
        /// <param name="context">El Microsoft.AspNetCore.Http.HttpContext para la solicitud actual.</param>
        /// <param name="next">El delegado que representa el middleware restante en la canalización de solicitud.</param>
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            bool ok = await base.Authenticate(context, _userRepository, _tokenBuilder, urlExceptions, _logger, urlSecure);
            if (ok)
            {
                await next(context);
            }
        }

    }

    #region ExtensionMethod
    /// <summary>
    /// Extensión para IApplicationBuilder "UseArenaIMiddlewareAuthorization". 
    /// </summary>
    public static class ArenaAuthorizationIMiddlewareExtension
    {
        /// <summary>
        /// Habilita uso UseArenaAuthorization. 
        /// </summary>
        /// <param name="app"><c>IApplicationBuilder</c></param>
        public static IApplicationBuilder UseArenaIMiddlewareAuthorization(this IApplicationBuilder app)
        {
            app.UseMiddleware<ArenaAuthenticationIMiddleware>( );
            return app;
        }
    }
    #endregion

}