﻿namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Respuesta estándar de seguridad para eventos finalizados. 
    /// </summary>
    public class SecurityResponse
    {
        /// <value>Indica si la operación finalizó correctamente.</value>
        public bool Success { get; set; }
    }
}
