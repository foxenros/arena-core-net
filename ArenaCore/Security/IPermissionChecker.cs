﻿using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Interfaz para autorización de permisos. 
    /// </summary>
    public interface IPermissionChecker
    {
        /// <summary>
        /// Devuelve si autoriza la entrada al ítem de menú. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        bool CheckPermission(string userName, string menuItId);

        /// <summary>
        /// Devuelve si autoriza la entrada al ítem de menú en la acción indicada. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <param name="actionCode">Acción <c>ActionWorkWithEnum</c>.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        bool CheckPermission(string userName, string menuItId, ActionWorkWithEnum actionCode);

        /// <summary>
        /// Devuelve si autoriza la entrada al ítem de menú y el acceso indicado. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <param name="accessCode">Codigo de acceso.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        bool CheckPermission(string userName, string menuItId, string accessCode);

        /// <summary>
        /// Devuelve si autoriza la entrada a la lista asociada al ítem de menú. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        /// <param name="menuItId">Ítem de menú.</param>
        /// <returns>
        /// Si es autorizado.
        /// </returns>
        bool CheckListPermission(string userName, string menuItId);

        /// <summary>
        /// Limpia el cache. 
        /// </summary>
        /// <param name="userName">Nombre de usuario.</param>
        void ClearUserCache(string userName);
    }
}