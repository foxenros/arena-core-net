﻿using ArenaSoftware.ArenaCore.Commons;
using System;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Clase <c>ArenaException</c> sobre autorización. 
    /// </summary>
    public class ArenaUnauthorizedException : ArenaException
    {
        /// <summary>
        /// Constructor sin parámetros. 
        /// </summary>
        public ArenaUnauthorizedException()
        {
        }

        /// <summary>
        /// Constructor con mensaje. 
        /// </summary>
        /// <param name="message">Mensaje de error.</param>
        public ArenaUnauthorizedException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructor con mensaje y <c>Exception</c>. 
        /// </summary>
        /// <param name="message">Mensaje de error.</param>
        /// <param name="inner"><c>Exception</c> que disparó.</param>
        public ArenaUnauthorizedException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
