﻿using ArenaSoftware.ArenaCore.Logs;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Soporte para autenticación asincrónica por token del contexto <c>HttpContext</c>. 
    /// </summary>
    public class ArenaAuthentication
    {
        /// <summary>
        /// Método para autenticar.
        /// </summary>
        /// <param name="context">El Microsoft.AspNetCore.Http.HttpContext para la solicitud actual.</param>
        /// <param name="userRepository">Repositorio de usuarios.</param>
        /// <param name="tokenBuilder">Generador de tokens.</param>
        /// <param name="urlExceptions">String con URLs exceptuadas, separadas por comas.</param>
        /// <param name="logger">Logger.</param>
        /// <param name="urlSecure">String con URLs a autorizar, separadas por comas.</param>
        protected async Task<bool> Authenticate(HttpContext context, 
            IUserRepository userRepository, 
            ITokenBuilder tokenBuilder, 
            string[] urlExceptions, 
            ILogger logger,
            string[] urlSecure)
        {

            bool checkUrl = false;

            string requestUrl = context.Request.Path.Value;
            if (requestUrl.StartsWith("/Account/"))
            {
                logger.LogDebug(LoggingEvents.Security, "URL [{0}] '/Account/' Route!!!", requestUrl);
            }
            else
            {
                if (urlSecure != null)
                {
                    foreach (string url in urlSecure)
                    {
                        if (requestUrl.StartsWith(url))
                        {
                            checkUrl = true;
                            break;
                        }
                    }
                }

                if (urlExceptions != null)
                {
                    foreach (string url in urlExceptions)
                    {
                        if (url == requestUrl || url == requestUrl + "/")
                        {
                            checkUrl = false;
                            break;
                        }
                        else if (url.EndsWith("*"))
                        {
                            // Revisa urls con el comodín '*'.
                            string endUrl = url.Substring(0, url.Length - 1);
                            if (requestUrl.StartsWith(endUrl))
                            {
                                logger.LogDebug(LoggingEvents.Security, "URL [{0}] exceptuada para autorización", requestUrl);
                                checkUrl = false;
                                break;
                            }
                        }
                    }

                }
            }

            if (checkUrl)
            {
                bool auth = false;
                string username = null;
                string authToken = ExtractAuthTokenFromRequest(context);
                if(authToken != null)
                {
                    username = tokenBuilder.UsernameFromToken(authToken);
                    string company = tokenBuilder.CompanyFromToken(authToken);
                    string origin = tokenBuilder.OriginFromToken(authToken);

                    IUser user = userRepository.GetItem(username);
                    if (user.Inactive)
                    {
                        auth = false;
                    }
                    else
                    {
                        BasicAuthenticationIdentity identity = new BasicAuthenticationIdentity(username, user.Password, company, origin);

                        // ValidateToken valida la contraseña y su expiración. 
                        if (identity != null && tokenBuilder.ValidateToken(authToken, identity))
                        {
                            // Auntenticado.
                            var genericPrincipal = new GenericPrincipal(identity, null);
                            Thread.CurrentPrincipal = genericPrincipal;

                            //Task taskSignIn = SignIn(context, username);
                            //await taskSignIn;
                            auth = true;
                        }
                    }
                }
                if (!auth)
                {
                    // UnAuthorized.
                    logger.LogDebug(LoggingEvents.Security, "URL [{0}] no autorizada para {1} ", requestUrl, username);
                    context.Response.StatusCode = 401;
                    //Task taskSignOut = SignOut(context);
                    //await taskSignOut;
                    return false;
                }
            }

            return true;
        }

        private string ExtractAuthTokenFromRequest(HttpContext httpContext)
        {
            string authToken = httpContext.Request.Headers["X-Auth-Token"];
            return authToken;
        }

        /*
        private async Task SignIn(HttpContext context, string userName)
        {
            var claims = new List<Claim>
            {
                new Claim("userName", userName)
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            Task task = AuthenticationHttpContextExtensions.SignInAsync(
                context,
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity));

            await task;
        }

        private async Task SignOut(HttpContext context)
        {
            await AuthenticationHttpContextExtensions.SignOutAsync(context);
        }
        */

    }

}
