﻿using System.Collections.Generic;

namespace ArenaSoftware.ArenaCore.Security
{
    /// <summary>
    /// Clase con información de acceso de un ítem de menú. 
    /// </summary>
    public class ProfileItem
    {
        /// <value>Código de ítem de menú.</value>
        public string MenuItId { get; set; }

        /// <value>Indica si es permitido el ingreso.</value>
        public bool Enabled { get; set; }

        /// <value>Diccionario con los accesos de seguridad.</value>
        public Dictionary<string, ProfileItemAccess> ProfileItemAccess { get; set; } = new Dictionary<string, ProfileItemAccess>();
    }
}
