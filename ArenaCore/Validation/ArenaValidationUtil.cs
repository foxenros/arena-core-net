﻿using ArenaSoftware.ArenaCore.Filter;
using ArenaSoftware.ArenaCore.Logs;
using ArenaSoftware.ArenaCore.Security;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase útil para validaciones de negocio. 
    /// </summary>
    public class ArenaValidationUtil
    {
        /// <value>Servicio de contexto <c>IServiceProvider</c>.</value>
        public readonly IServiceProvider _serviceProvider;

        /// <value>Logger.</value>
        protected ILogger Logger;

        /// <summary>
        /// Constructor con servicio de contexto y logger. 
        /// </summary>
        /// <param name="serviceProvider">Servicio de contexto.</param>
        /// <param name="logger">Logger.</param>
        public ArenaValidationUtil(IServiceProvider serviceProvider, ILogger<ArenaValidationUtil> logger)
        {
            _serviceProvider = serviceProvider;
            this.Logger = logger;
        }

        /// <summary>
        /// Ejecuta la validación del ítem. 
        /// </summary>
        /// <param name="item">Ítem de negocio.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        public ArenaValidationResponse<T> TryValidateObject<T>(T item)
        {
            return TryValidateObject(item, ActionWorkWithEnum.GET);
        }

        /// <summary>
        /// Ejecuta la validación del ítem para una acción determinada. 
        /// </summary>
        /// <param name="item">Ítem de negocio.</param>
        /// <param name="action">Acción <c>ActionWorkWithEnum</c>.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        public ArenaValidationResponse<T> TryValidateObject<T>(T item, ActionWorkWithEnum action)
        {
            return TryValidateObject(item, action, 0);
        }

        /// <summary>
        /// Ejecuta la validación del ítem para una acción determinada informando el subíndice dentro de la colección. 
        /// </summary>
        /// <param name="item">Ítem de negocio.</param>
        /// <param name="action">Acción <c>ActionWorkWithEnum</c>.</param>
        /// <param name="orderCollectionNumber">Subíndice.</param>
        /// <param name="group">Grupo de validación.</param>
        /// <param name="validateOnDelete">Indica que se debe validar en operaciones ActionWorkWithEnum.DELETE, defecto false.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        public ArenaValidationResponse<T> TryValidateObject<T>(T item, ActionWorkWithEnum action, int orderCollectionNumber, string group = "", 
            bool validateOnDelete = false)
        {
            ArenaValidationResponse<T> avr = new ArenaValidationResponse<T>();
            avr.ResultObject = item;
            if(action != ActionWorkWithEnum.DELETE || validateOnDelete)
            {
                var context = new ValidationContext(item, serviceProvider: _serviceProvider, items: null);
                var results = new List<ValidationResult>();

                var isValid = Validator.TryValidateObject(item, context, results, true);

                if (!isValid)
                {
                    ProccesValidationResult(avr, results, orderCollectionNumber, group);
                }

            }

            return avr;
        }

        private ArenaValidationResponse<T> TryValidateValue<T>(object value, 
            ValidationContext validationContext, 
            ICollection<ValidationResult> validationResults, 
            IEnumerable<ValidationAttribute> validationAttributes, 
            int orderCollectionNumber, 
            string group = "")
        {
            ArenaValidationResponse<T> avr = new ArenaValidationResponse<T>();
            var isValid = Validator.TryValidateValue(value, validationContext, validationResults, validationAttributes);
            if (validationContext.ObjectInstance != null)
            {
                T item = (T) validationContext.ObjectInstance;  
                avr.ResultObject = item;
            }

            if (!isValid)
            {
                ProccesValidationResult(avr, validationResults, orderCollectionNumber, group);
            }

            return avr;
        }

        private void ProccesValidationResult<T>(ArenaValidationResponse<T> avr, ICollection<ValidationResult> validationResults, int orderCollectionNumber, string group = "")
        {
            foreach (var validationResult in validationResults)
            {
                ArenaError ae = new ArenaError();
                if (validationResult is ArenaValidationResult)
                {
                    ArenaValidationResult arenaValidationResult = (ArenaValidationResult)validationResult;
                    ae.FieldName = arenaValidationResult.FieldName;
                    ae.MsgErrorDescription = arenaValidationResult.ErrorMessage;
                    ae.MsgErrorKey = arenaValidationResult.MsgErrorKey;
                    ae.Group = group;
                }
                else
                {
                    ae.FieldName = validationResult.ErrorMessage;
                    ae.MsgErrorDescription = validationResult.ErrorMessage;
                    ae.MsgErrorKey = validationResult.ErrorMessage;
                    ae.Group = group;
                }

                // Número de línea para las colecciones (comienza en 1, 0 significa sin número).
                ae.OrderCollectionNumber = orderCollectionNumber;

                avr.Errors.Add(ae);
            }
        }

        /// <summary>
        /// Realiza una validación mediante la información de <c>ValidationInfo</c>.
        /// </summary>
        /// <param name="validationInfo">Información para realizar la validación.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        public ArenaValidationResponse Validate(ValidationInfo validationInfo)
        {
            // Obtiene el ensamblado del cliente.
            Assembly assembly = validationInfo.Assembly;

            this.Logger.LogDebug(LoggingEvents.Validation, "Assembly={0}", assembly);

            // Obtiene una instancia de clase validadora.
            List<ValidationAttribute> attr = new List<ValidationAttribute>();

            var type = assembly.GetType(validationInfo.MethodName);
            object obj = Activator.CreateInstance(type);
            this.Logger.LogDebug(LoggingEvents.Validation, "Type ValidationAttribute={0}", type);

            ValidationAttribute va = (ValidationAttribute) obj;
            attr.Add(va);

            // Lee los atributos de ArenaValidationAttribute de la clase validadora.
            var dnAttribute = va.GetType().GetCustomAttributes(
            typeof(ArenaValidationAttribute), true).FirstOrDefault() as ArenaValidationAttribute;

            // Crea el ítem del tipo TypeName indicado en ValidationAttribute.
            var typeItem = assembly.GetType(dnAttribute.TypeName);
            object item = Activator.CreateInstance(typeItem);
            this.Logger.LogDebug(LoggingEvents.Validation, "TypeItem={0}", typeItem);

            // Crea el diccionario con los valores
            IDictionary<object, object> items = new Dictionary<object, object>();
            object firstValue = null;

            int n = 0;
            foreach (ArenaProperty arenaProperty in dnAttribute.ArenaProperties)
            {
                // Parsea el valor al tipo.
                object value = arenaProperty.GetParseValue(validationInfo.ValidatorParams[n++].Value, (ArenaFormat) validationInfo);
                if(firstValue == null)
                {
                    firstValue = value;
                }
                
                // Asigna la propiedad.
                item.GetType().GetProperty(arenaProperty.PropertyName).SetValue(item, value, null);

                // Guarda los demás valores.
                items.Add(arenaProperty.PropertyName, value);
            }

            this.Logger.LogDebug(LoggingEvents.Validation, "Context [Item={0}, Items={2}]", item, ArenaLogUtil.IEnumerableToString(items));
            var context = new ValidationContext(item, serviceProvider: _serviceProvider, items: items);
            var results = new List<ValidationResult>();

            // Llama a la validación.
            this.Logger.LogDebug(LoggingEvents.Validation, "FirstValue={0}", firstValue);
            var avr = this.TryValidateValue<object>(firstValue, context, results, attr, validationInfo.OrderCollectionNumber, validationInfo.Group);

            return avr;
        }

    }
}
