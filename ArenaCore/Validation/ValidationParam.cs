﻿namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase con información de parámetros para validación de negocio. 
    /// </summary>
    public class ValidationParam
    {
        /// <value>Tipo del parámetro.</value>
        public string Type { set; get; }

        /// <value>Valor.</value>
        public string Value { set; get; }

        /// <value>Valor como objeto <c>object</c>.</value>
        public object ObjectValue { set; get; }

        /// <summary>
        /// ToString para logging. 
        /// </summary>
        public override string ToString()
        {
            return "ValidationParam=[" +
                "Type=" + Type + ", " +
                "Value=" + Value + ", " +
                "ObjectValue=" + ObjectValue.ToString() + 
                "]";
        }

    }
}