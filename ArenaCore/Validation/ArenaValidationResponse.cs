﻿using System;
using System.Collections.Generic;

namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase con el resultado de la validación de un ítem de negocio. 
    /// </summary>
    public class ArenaValidationResponse
    {
        /// <value>Errores.</value>
        public List<ArenaError> Errors { get; set; } = new List<ArenaError>();

        /// <value>Advertencias.</value>
        public List<ArenaError> Warnings { get; set; } = new List<ArenaError>();

        /// <value>Indica si la validación NO terminó de evaluarse por completo.</value>
        public bool ValidationBroken;

        /// <value>Ítem resultado de la validación.</value>
        public Object ResultObject { get; set; }

        /// <value>Indica si hubo errores.</value>
        public bool HasErrors()
        {
            return (Errors.Count > 0);
        }

        /// <summary>
        /// Constructor con otro <c>ArenaValidationResponse</c>. 
        /// </summary>
        /// <param name="other"><c>ArenaValidationResponse</c></param>
        public void AddArenaValidationResponse(ArenaValidationResponse other)
        {
            foreach(ArenaError ae in other.Errors)
            {
                Errors.Add(ae);
            }
            foreach (ArenaError ae in other.Warnings)
            {
                Warnings.Add(ae);
            }
        }
    }

    /// <summary>
    /// Clase con tipo que incluye el resultado de la validación de un ítem de negocio. 
    /// </summary>
    /// <typeparam name="E">Tipo del <c>ResultObject</c> .</typeparam>
    public class ArenaValidationResponse<E> : ArenaValidationResponse
    {
        /// <value>Ítem resultado de la validación.</value>
        public new E ResultObject { get; set; }
    }
}