﻿using ArenaSoftware.ArenaCore.Filter;
using ArenaSoftware.ArenaCore.Logs;
using System.Collections.Generic;
using System.Reflection;

namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase con información para validación de negocio. 
    /// </summary>
    public class ValidationInfo : ArenaFormat
    {
        /// <value>Nombre de clase de validación.</value>
        public string MethodName { set; get; }

        /// <value>Lista de parámetros de validación.</value>
        public List<ValidationParam> ValidatorParams { set; get; }

        /// <value>Ensamblado de la clase validadora.</value>
        public Assembly Assembly { set; get; }

        /// <value>Indice en caso de colección.</value>
        public int OrderCollectionNumber { set; get; }

        /// <value>Grupo de validación.</value>
        public string Group { set; get; }

    /// <summary>
        /// ToString para logging. 
        /// </summary>
        public override string ToString()
        {
            return "ValidationInfo=[" +
                "MethodName=" + MethodName + ", " +
                "ValidatorParams=" + ArenaLogUtil.IEnumerableToString(ValidatorParams) + ", " +
                "Assembly=" + Assembly + 
                "]";
        }
    }
}