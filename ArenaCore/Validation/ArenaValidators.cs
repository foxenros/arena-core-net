﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase de validación ArenaRequired (reemplaza <c>Required</c>). 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ArenaRequired : ValidationAttribute
    {
        /// <value>Clave del error.</value>
        public string MsgErrorKey { get; set; }

        /// <value>Nombre del campo.</value>
        public string FieldName { get; set; }

        /// <summary>
        /// Determina cuando un valor de objeto es válido. 
        /// </summary>
        /// <param name="value">Valor.</param>
        /// <param name="validationContext">Cotexto de validación.</param>
        /// <returns>
        /// Verdadero si es válido.
        /// </returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // TODO: ver para diferentes tipos.
            if (value == null || value.ToString() == "")
            {
                return new ArenaValidationResult(ErrorMessage, MsgErrorKey, FieldName);
            }
            return ValidationResult.Success;
        }
    }
}
