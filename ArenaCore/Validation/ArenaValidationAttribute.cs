﻿using ArenaSoftware.ArenaCore.Filter;
using System;

namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase de atributos con información para validación. 
    /// </summary>
    public class ArenaValidationAttribute : Attribute
    {
        /// <summary>
        /// Constructor con nombre de tipo del ítem y propiedades <c>ArenaProperty</c>. 
        /// </summary>
        /// <param name="typeName">Nombre de tipo del ítem.</param>
        /// <param name="arenaProperties">Propiedades <c>ArenaProperty</c>.</param>
        public ArenaValidationAttribute(string typeName, ArenaProperty[] arenaProperties)
        {
            this.TypeName = typeName;
            this.ArenaProperties = arenaProperties;
        }

        /// <summary>
        /// Constructor con el tipo del ítem, nombre de la propiedad y el tipo de la propiedad. 
        /// </summary>
        /// <param name="typeName">Nombre de tipo del ítem.</param>
        /// <param name="propertyName">Nombre de propiedad.</param>
        /// <param name="propertyTypeName">Tipo de la propiedad.</param>
        public ArenaValidationAttribute(string typeName, string propertyName, string propertyTypeName)
        {
            this.TypeName = typeName;
            this.ArenaProperties = new ArenaProperty[] { new ArenaProperty(propertyName, propertyTypeName) };
        }

        /// <value>Nombre de tipo del ítem.</value>
        public string TypeName { get; set; }

        /// <value>Propiedades <c>ArenaProperty</c>.</value>
        public ArenaProperty[] ArenaProperties { get; set; }
    }

    /// <summary>
    /// Clase con información de propiedades para <c>ArenaValidationAttribute</c>. 
    /// </summary>
    public class ArenaProperty
    {
        /// <summary>
        /// Constructor con nombre de propiedad y tipo de la propiedad. 
        /// </summary>
        /// <param name="propertyName">Nombre de propiedad.</param>
        /// <param name="propertyTypeName">Tipo de la propiedad.</param>
        public ArenaProperty(string propertyName, string propertyTypeName)
        {
            this.PropertyName = propertyName;
            this.PropertyTypeName = propertyTypeName;
        }

        /// <value>Nombre de propiedad.</value>
        public string PropertyName { get; set; }

        /// <value>Tipo de la propiedad.</value>
        public string PropertyTypeName { get; set; }

        /// <summary>
        /// Devuelve el valor parseado al tipo. 
        /// </summary>
        /// <returns>
        /// El valor parseado al tipo.
        /// </returns>
        public Object GetParseValue(object value, ArenaFormat arenaFormat)
        {
            // TODO: GetParseValue, mejorar, mejorar y mejorar. Utilizar arenaFormat.
            string typeLower = this.PropertyTypeName.ToLower();
            if (typeLower == "int")
            {
                return int.Parse(value.ToString());
            }
            else if (typeLower == "double")
            {
                return double.Parse(value.ToString());
            }
            else if (typeLower == "string")
            {
                return value.ToString();
            }
            else if (typeLower == "datetime")
            {
                return DateTime.Parse(value.ToString());
            }
            else
            {
                return null;
            }

        }
    }
}
