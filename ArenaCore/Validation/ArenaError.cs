﻿using ArenaSoftware.ArenaCore.Logs;

namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase con información de un error a nivel de negocio. 
    /// </summary>
    public class ArenaError
    {
        /// <value>Clave del error.</value>
        public string MsgErrorKey {get; set; }

        /// <value>Mensaje del error.</value>
        public string MsgErrorDescription { get; set; }

        /// <value>Nombre del campo.</value>
        public string FieldName { get; set; }

        /// <value>Valores asociados.</value>
        public object[] Values { get; set; }

        /// <value>Índice dentro de la colección (si corresponde).</value>
        public int OrderCollectionNumber { get; set; } = 0;

        /// <value>Grupo de validación.</value>
        public string Group { get; set; }

        /// <summary>
        /// Constructor sin parámetros. 
        /// </summary>
        public ArenaError()
        {
        }

        /// <summary>
        /// Constructor con clave de error y mensaje de error. 
        /// </summary>
        /// <param name="msgErrorKey">Código del error.</param>
        /// <param name="msgErrorDescription">Mensaje del error.</param>
        public ArenaError(string msgErrorKey, string msgErrorDescription)
        {
            this.MsgErrorKey = msgErrorKey;
            this.MsgErrorDescription = msgErrorDescription;
        }

        /// <summary>
        /// Constructor con clave de error, mensaje de error y valores asociados. 
        /// </summary>
        /// <param name="msgErrorKey">Código del error.</param>
        /// <param name="msgErrorDescription">Mensaje del error.</param>
        /// <param name="values">Valores asociados.</param>
        public ArenaError(string msgErrorKey, string msgErrorDescription, object[] values) : this(msgErrorKey, msgErrorDescription)
        {
            this.Values = values;
        }

        /// <summary>
        /// Constructor con clave de error, mensaje de error y nombre del campo. 
        /// </summary>
        /// <param name="msgErrorKey">Código del error.</param>
        /// <param name="msgErrorDescription">Mensaje del error.</param>
        /// <param name="fieldName">Nombre del campo.</param>
        public ArenaError(string msgErrorKey, string msgErrorDescription, string fieldName) : this(msgErrorKey, msgErrorDescription)
        {
            this.FieldName = fieldName;
        }

        /// <summary>
        /// Constructor con clave de error, mensaje de error, nombre de campo y valores asociados. 
        /// </summary>
        /// <param name="msgErrorKey">Código del error.</param>
        /// <param name="msgErrorDescription">Mensaje del error.</param>
        /// <param name="fieldName">Nombre del campo.</param>
        /// <param name="values">Valores asociados.</param>
        public ArenaError(string msgErrorKey, string msgErrorDescription, string fieldName, object[] values) : this(msgErrorKey, msgErrorDescription, fieldName)
        {
            this.Values = values;
        }

        /// <returns>
        /// ToString con los atributos.
        /// </returns>
        public override string ToString()
        {
            return "ArenaError=[" +
                "MsgErrorKey=" + MsgErrorKey + ", " +
                "MsgErrorDescription=" + MsgErrorDescription + ", " +
                "FieldName=" + FieldName + ", " +
                "Values=" + ArenaLogUtil.IEnumerableToString(Values) + ", " +
                "OrderCollectionNumber=" + OrderCollectionNumber+
                "]";
        }

    }
}
