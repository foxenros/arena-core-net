﻿using System.ComponentModel.DataAnnotations;

namespace ArenaSoftware.ArenaCore.Validation
{
    /// <summary>
    /// Clase que extiende <c>ValidationResult</c> y agrega información de clave y nombre de campo. 
    /// </summary>
    public class ArenaValidationResult : ValidationResult
    {
        /// <summary>
        /// Constructor con mensaje de error. 
        /// </summary>
        /// <param name="errorMessage">Mensaje de error.</param>
        public ArenaValidationResult(string errorMessage) : base(errorMessage)
        {
        }

        /// <summary>
        /// Constructor con mensaje de error, clave y nombre de campo. 
        /// </summary>
        /// <param name="errorMessage">Mensaje de error.</param>
        /// <param name="msgErrorKey">Clave del error.</param>
        /// <param name="fieldName">Nombre del campo.</param>
        public ArenaValidationResult(string errorMessage, string msgErrorKey, string fieldName) : base(errorMessage)
        {
            this.MsgErrorKey = msgErrorKey;
            this.FieldName = fieldName;
        }

        /// <value>Clave del error.</value>
        public string MsgErrorKey { get; set; }

        /// <value>Nombre del campo.</value>
        public string FieldName { get; set; }
    }
}
