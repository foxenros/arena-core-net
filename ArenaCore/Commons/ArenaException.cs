﻿using System;

namespace ArenaSoftware.ArenaCore.Commons
{
    /// <summary>
    /// Clase <c>Exception</c> general de la librería ArenaCore. 
    /// </summary>
    public class ArenaException : Exception
    {
        /// <summary>
        /// Constructor sin parámetros. 
        /// </summary>
        public ArenaException()
        {
        }

        /// <summary>
        /// Constructor con mensaje. 
        /// </summary>
        /// <param name="message">Mensaje de error.</param>
        public ArenaException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructor con mensaje y <c>Exception</c>. 
        /// </summary>
        /// <param name="message">Mensaje de error.</param>
        /// <param name="inner"><c>Exception</c> que disparó.</param>
        public ArenaException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}
