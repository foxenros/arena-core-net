﻿using System.Collections;

namespace ArenaSoftware.ArenaCore.DataAccess
{
    /// <summary>
    /// Clase con información de una expresión lógica "where" y sus correspondientes parámetros asociados. 
    /// </summary>
    public class QueryWhere
    {
        /// <value>Texto "where" con la expresión.</value>
        public string Where { get; set; }

        /// <value>Parámetros de la expresión.</value>
        public IList Parameters { get; set; }
    }
}
