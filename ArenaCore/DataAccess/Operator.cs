﻿using System.Collections.Generic;

namespace ArenaSoftware.ArenaCore.DataAccess
{
    /// <summary>
    /// Clase para el enumerador <c>OperatorEnum</c>. 
    /// </summary>
    public class Operator
    {
        /// <summary>
        /// Enum para operadores lógicos de expresiones estructuradas. 
        /// </summary>
        public enum OperatorEnum
        {
            /// <value>Ninguno.</value>
            NOTHING,

            /// <value>Igual.</value>
            EQUAL,
            
            /// <value>Distinto.</value>
            NOTEQUAL,
            
            /// <value>Mayor a.</value>
            GREATERTHAN,
            
            /// <value>Menor a.</value>
            LESSTHAN,
            
            /// <value>Mayor o igual a.</value>
            GREATERTHAN_OR_EQUALTO,
            
            /// <value>Menor o igual a.</value>
            LESSTHAN_OR_EQUALTO,
            
            /// <value>Dentro de [cadena de texto con la lista", Ej. var= "(1, 2, 3)"].</value>
            IN,
            
            /// <value>Dentro de lista.</value>
            LIST,

            /// <value>Fuera de la lista.</value>
            NOT_LIST,
            
            /// <value>Contiene.</value>
            LIKE,
            
            /// <value>Comienza con.</value>
            START_WITH,
            
            /// <value>Termina con.</value>
            END_WITH,
            
            /// <value>Entre.</value>
            BETWEEN,

            /// <value>No está entre.</value>
            NOT_BETWEEN,
            
            /// <value>Es nulo</value>
            ISNULL,
            
            /// <value>No es nulo</value>
            ISNOTNULL
        }

        internal class OperatorObj
        {

            public OperatorObj(OperatorEnum op, string opString, string expression, int operatorsCount)
            {
                this.Op = op;
                this.OpString = opString;
                this.Expression = expression;
                this.OperatorsCount = operatorsCount;
            }

            public OperatorEnum Op { set; get; }
            public string OpString { set; get; }
            public string Expression { set; get; }
            public int OperatorsCount { set; get; }
        }

        private static Dictionary<string, OperatorObj> operators;

        static Operator() {
            operators = new Dictionary<string, OperatorObj>
            {
                { "", new OperatorObj(OperatorEnum.NOTHING, "", "", 0) },
                { "=", new OperatorObj(OperatorEnum.EQUAL, "=", "{f} = ?", 1) },
                { "<>", new OperatorObj(OperatorEnum.NOTEQUAL, "<>", "{f} <> ?", 1) },
                { ">", new OperatorObj(OperatorEnum.GREATERTHAN, ">", "{f} > ?", 1) },
                { "<", new OperatorObj(OperatorEnum.LESSTHAN, "<", "{f} < ?", 1) },
                { ">=", new OperatorObj(OperatorEnum.GREATERTHAN_OR_EQUALTO, ">=", "{f} >= ?", 1) },
                { "<=", new OperatorObj(OperatorEnum.LESSTHAN_OR_EQUALTO, "<=", "{f} <= ?", 1) },
                { "IN", new OperatorObj(OperatorEnum.IN, "IN", "{f} IN ({v})", 1) },
                { "LIST", new OperatorObj(OperatorEnum.LIST, "LIST", "{f} IN ({l})", -1) },
                { "NOT_LIST", new OperatorObj(OperatorEnum.NOT_LIST, "NOT_LIST", "{f} NOT IN ({l})", -1) },
                { "LIKE", new OperatorObj(OperatorEnum.LIKE, "LIKE", "{f}.Contains(?)", 1) },
                { "START_WITH", new OperatorObj(OperatorEnum.START_WITH, "START_WITH", "{f}.StartsWith(?)", 1) },
                { "END_WITH", new OperatorObj(OperatorEnum.END_WITH, "END_WITH", "{f}.EndsWith(?)", 1) },
                { "BETWEEN", new OperatorObj(OperatorEnum.BETWEEN, "BETWEEN", "{f} BETWEEN ?0 AND ?1", 2) },
                { "NOT_BETWEEN", new OperatorObj(OperatorEnum.NOT_BETWEEN, "NOT_BETWEEN", "{f} NOT BETWEEN ?0 AND ?1", 2) },
                { "ISNULL", new OperatorObj(OperatorEnum.ISNULL, "ISNULL", "{f} IS NULL", 0) },
                { "ISNOTNULL", new OperatorObj(OperatorEnum.ISNOTNULL, "ISNOTNULL", "NOT {f} IS NULL", 0) }
            };
        }

        /// <summary>
        /// Devuelve el operador abreviado en formato string, ejemplo <c>OperatorEnum.EQUAL</c> pertenece a "=". 
        /// </summary>
        /// <param name="op">Enum <c>OperatorEnum</c>.</param>
        /// <returns>
        /// Operador abreviado en formato string.
        /// </returns>
        public static string GetOperator(OperatorEnum op)
        {
            foreach (OperatorObj opObj in operators.Values)
            {
                if (opObj.Op == op)
                {
                    return opObj.OpString;
                }
            }
            return "";
        }

        /// <summary>
        /// Devuelve la expresión relacionada al enum, ejemplo <c>OperatorEnum.EQUAL</c> pertenece a "{f} = ?". 
        /// </summary>
        /// <param name="op">Enum <c>OperatorEnum</c>.</param>
        /// <returns>
        /// Expresión relacionada al enum.
        /// </returns>
        public static string GetExpression(OperatorEnum op)
        {
            foreach (OperatorObj opObj in operators.Values)
            {
                if (opObj.Op == op)
                {
                    return opObj.Expression;
                }
            }
            return "";
        }

        /// <summary>
        /// Devuelve la cantidad de operadores lógicos relacionados al enum, ejemplo <c>OperatorEnum.EQUAL</c> contiene "1" y <c>OperatorEnum.BETWEEN</c> contiene "2". 
        /// </summary>
        /// <param name="op">Enum <c>OperatorEnum</c>.</param>
        /// <returns>
        /// Cantidad de operadores lógicos relacionados al enum.
        /// </returns>
        public static int GetOperatorsCount(OperatorEnum op)
        {
            foreach (OperatorObj opObj in operators.Values)
            {
                if (opObj.Op == op)
                {
                    return opObj.OperatorsCount;
                }
            }
            return 0;
        }

        /// <summary>
        /// Devuelve el enum correspondiente a la abreviación, ejemplo "=" pertenece a "OperatorEnum.EQUAL". 
        /// </summary>
        /// <param name="op">Abreviación del operador.</param>
        /// <returns>
        /// Enum <c>OperatorEnum</c> correspondiente a la abreviación.
        /// </returns>
        public static OperatorEnum GetOperatorEnum(string op)
        {
            if(operators.ContainsKey(op))
            {
                return operators[op].Op;
            } else
            {
                return OperatorEnum.NOTHING;
            }
        }

    }
}
