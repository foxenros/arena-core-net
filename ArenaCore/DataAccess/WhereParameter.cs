﻿using ArenaSoftware.ArenaCore.Logs;
using System.Linq;
using static ArenaSoftware.ArenaCore.DataAccess.Operator;

namespace ArenaSoftware.ArenaCore.DataAccess
{
    /// <summary>
    /// Clase con información de parámetro de expresión lógica estructurada. 
    /// </summary>
    public abstract class WhereParameter
    {
        /// <value>Valores como <c>object[]</c>.</value>
        protected object[] valuesUntyped;

        /// <value>Nombre del campo.</value>
        public string FieldName { get; set; }

        /// <value>Operador <c>OperatorEnum</c>.</value>
        public OperatorEnum Op { get; set; }

        /// <value>Expresión.</value>
        public string Expression { get; set; }

        /// <value>Indica si de debe forzar a Uppercase.</value>
        public bool ForceUppercase { get; set; }

        /// <value>Indica si es una expresión.</value>
        public bool IsExpression()
        {
            return !string.IsNullOrEmpty(Expression);
        }

        /// <returns>
        /// Valores almacenados como <c>object[]</c>.
        /// </returns>
        public object[] ValuesUntyped {
            get { return valuesUntyped;  }
        }

        /// <returns>
        /// Primer valor almacenado como <c>object[0]</c>.
        /// </returns>
        public object ValueUntyped {
            get  { return valuesUntyped[0]; }
        }

        /// <returns>
        /// ToString con los atributos.
        /// </returns>
        public override string ToString()
        {
            return "WhereParameter=[" +
                "FieldName=" + FieldName + ", " +
                "Values=" + ArenaLogUtil.IEnumerableToString(ValuesUntyped) + ", " +
                "Op=" + Op + ", " +
                "Expression=" + Expression + ", " +
                "ForceUppercase=" + ForceUppercase.ToString() +
                "]";
        }
    }

    /// <summary>
    /// Clase con tipo de información de parámetro de expresión lógica estructurada. 
    /// </summary>
    /// <typeparam name="K">Tipo del parámetro.</typeparam>
    public class WhereParameter<K> : WhereParameter
    {
        private K[] values;

        /// <value>Valores con tipo.</value>
        public K[] Values {
            get {
                return this.values;
            }
            set {
                this.values = value;
                valuesUntyped = this.values.Cast<object>().ToArray(); ;
            }
        }

        /// <value>Primer valor con tipo.</value>
        public K Value { get => Values[0]; set => Values[0] = value; }

        /// <summary>
        /// Constructor con campo, operador y valores. 
        /// </summary>
        /// <param name="fieldName">Campo.</param>
        /// <param name="op">Operador.</param>
        /// <param name="values">Valores.</param>
        public WhereParameter(string fieldName, OperatorEnum op, params K[] values)
        {
            this.Values = values;
            this.FieldName = fieldName;
            this.Op = op;
        }

        /// <summary>
        /// Constructor con campo, operador y valores. 
        /// </summary>
        /// <param name="fieldName">Campo.</param>
        /// <param name="op">Operador.</param>
        /// <param name="expression">Expresión.</param>
        /// <param name="forceUppercase">Fuerza Uppercase.</param>
        /// <param name="values">Valores.</param>
        public WhereParameter(string fieldName, OperatorEnum op, string expression, bool forceUppercase, params K[] values)
        {
            this.Values = values;
            this.FieldName = fieldName;
            this.Op = op;
            this.Expression = expression;
            this.ForceUppercase = forceUppercase;
        }

    }
}
