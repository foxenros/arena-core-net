﻿using ArenaSoftware.ArenaCore.Logs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using static ArenaSoftware.ArenaCore.DataAccess.Operator;

namespace ArenaSoftware.ArenaCore.DataAccess
{
    /// <summary>
    /// Clase generadora de expresiones lógicas <c>QueryWhere</c> a partír de una lista de <c>WhereParameter</c>
    /// </summary>
    public class QueryWhereBuilder
    {
        /// <value>Logger.</value>
        protected ILogger Logger;

        /// <summary>
        /// Constructor con logger. 
        /// </summary>
        /// <param name="logger">Logger.</param>
        public QueryWhereBuilder(ILogger<QueryWhereBuilder> logger)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Método para construir un <c>QueryWhere</c> con una lista de parámetros <c>WhereParameter</c>. 
        /// </summary>
        /// <param name="parameters">Lista de parámetros <c>WhereParameter</c>.</param>
        /// <returns>
        /// Objeto <c>QueryWhere</c> con la expresión.
        /// </returns>
        public QueryWhere Build(List<WhereParameter> parameters)
        {
            QueryWhere queryData = new QueryWhere();

            IList parametersRet = new ArrayList();
            StringBuilder where = new StringBuilder();
            int paramIndex = 0;

            foreach (WhereParameter whereParameter in parameters)
            {
                if (where.Length > 0)
                {
                    where.Append(" && ");
                }

                string expressionField;
                if (!whereParameter.IsExpression())
                {
                    // Es un campo literal.
                    expressionField = CheckForceFieldUpper(whereParameter.FieldName, whereParameter);
                }
                else
                {
                    // Es una expresión. Los caracteres @ serán reemplazados por el nombre del campo whereParameter.FieldName.
                    expressionField = CheckForceFieldUpper(whereParameter.Expression.Replace("@", whereParameter.FieldName), whereParameter);
                }
                if (whereParameter.Op == OperatorEnum.LIST)
                {
                    // Implementación para lista por parámetros.
                    StringBuilder listExpresion = new StringBuilder();
                    for (int n = 0; n <= whereParameter.ValuesUntyped.Length - 1; n++)
                    {
                        if (listExpresion.Length != 0)
                        {
                            listExpresion.Append(",");
                        }
                        listExpresion.Append("@" + paramIndex++);
                    }

                    string whereOp = Operator.GetExpression(whereParameter.Op);
                    whereOp = whereOp.Replace("{f}", expressionField);
                    whereOp = whereOp.Replace("{l}", listExpresion.ToString());
                    where.Append(whereOp);
                    this.Logger.LogDebug("Exp [{0}]", whereOp);
                }
                else if (whereParameter.Op == OperatorEnum.NOT_LIST)
                {

                    // Implementación para lista por parámetros.
                    StringBuilder listExpresion = new StringBuilder();
                    for (int n = 0; n <= whereParameter.ValuesUntyped.Length - 1; n++)
                    {
                        if (listExpresion.Length != 0)
                        {
                            listExpresion.Append(",");
                        }
                        listExpresion.Append("@" + paramIndex++);
                    }

                    string whereOp = Operator.GetExpression(whereParameter.Op);
                    whereOp = whereOp.Replace("{f}", expressionField);
                    whereOp = whereOp.Replace("{l}", listExpresion.ToString());
                    where.Append(whereOp);
                    this.Logger.LogDebug("Exp [{0}]", whereOp);
                }
                else if (whereParameter.Op == OperatorEnum.IN)
                {
                    // Lista de literales para expresión IN.
                    string whereOp = Operator.GetExpression(whereParameter.Op);

                    whereOp = whereOp.Replace("{f}", expressionField);
                    string valueString1 = (String) CheckForceValueUpper((whereParameter.ValuesUntyped[0]), whereParameter);

                    whereOp = whereOp.Replace("{v}", valueString1);

                    where.Append(whereOp);
                    this.Logger.LogDebug("Exp [{0}]", whereOp);
                }
                else
                {
                    // Implementación para unarios y binarios (EQUAL, BETWEEN, etc.)
                    string whereOp = Operator.GetExpression(whereParameter.Op);
                    whereOp = whereOp.Replace("{f}", expressionField);

                    int nOps = Operator.GetOperatorsCount(whereParameter.Op);
                    if(nOps == 1)
                    {
                        // Reemplaza el parámetro.
                        whereOp = whereOp.Replace("?", "@" + paramIndex++);
                    }
                    else if(nOps >= 1) {
                        // Reemplaza todos los parámetros.
                        for (int n = 0; n <= nOps - 1; n++)
                        {
                            whereOp = whereOp.Replace("?" + n, "@" + paramIndex++);
                        }
                    }
                    where.Append(whereOp);
                    this.Logger.LogDebug("Exp [{0}]", whereOp);
                }

                // Para todos menos IN (que son literales!) se pasan los parámetros.
                if (whereParameter.Op != OperatorEnum.IN && 
                    whereParameter.Op != OperatorEnum.ISNULL &&
                    whereParameter.Op != OperatorEnum.ISNOTNULL)
                {
                    int nOps = Operator.GetOperatorsCount(whereParameter.Op);
                    if (nOps == -1)
                    {
                        nOps = whereParameter.ValuesUntyped.Length;
                    }
                    // Pasa los parámetros...
                    for (int n = 0; n <= nOps - 1; n++)
                    {
                        Object val = whereParameter.ValuesUntyped[n];
                        val = CheckForceValueUpper(val, whereParameter);
                        parametersRet.Add(val);
                    }
                }
            }

            if (where.Length == 0)
            {
                where.Append("1=1");
            }

            queryData.Where = where.ToString();
            this.Logger.LogDebug(LoggingEvents.FilterData, "Where [{0}]", queryData.Where);

            queryData.Parameters = parametersRet;
            this.Logger.LogDebug(LoggingEvents.FilterData, "Parameters {0}", ArenaLogUtil.IEnumerableToString(parametersRet));

            return queryData;
        }


        private string CheckForceFieldUpper(string fieldName, WhereParameter whereParameter)
        {
            bool forceUppercase = whereParameter.ForceUppercase;
            if (forceUppercase && whereParameter.ValueUntyped is string){
                return fieldName + ".ToUpper()";
            } else {
                return fieldName;
            }
        }

        private object CheckForceValueUpper(object value, WhereParameter whereParameter)
        {
            bool forceUppercase = whereParameter.ForceUppercase;
            if (forceUppercase && whereParameter.ValueUntyped is string){
                string stringValue = ((string)value).ToUpper();

                return stringValue;
            } else {
                return value;
            }
        }

    }
}
