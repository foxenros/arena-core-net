﻿using System;

namespace ArenaSoftware.ArenaCore.Filter
{
    /// <summary>
    /// Clase con información de un filtro de campo para un <c>ArenaFilter</c>. 
    /// </summary>
    public class ArenaFieldFilter
    {
        /// <value>Nombre del campo.</value>
        public string FieldName { get; set; }

        /// <value>Operador en formato abreviado.</value>
        public string Op { get; set; }

        /// <value>Tipo en formato string, ejemplo: "string, "int, "double, "datetime", etc.</value>
        public string DataType { get; set; }

        /// <value>Valores.</value>
        public string[] Values { get; set; }

        /// <value>Cadena con una expresión válida para LINQ.</value>
        public string Expression { get; set; } = "";

        /// <value>Indica si se fuerza Uppercase.</value>
        public Nullable<bool> ForceUppercase { get; set; }

        /// <value>Indica para el caso de campos fecha si contiene horas, minutos y segundos.</value>
        public bool EnableDateWithTime { get; set; } = true;

    }
}
