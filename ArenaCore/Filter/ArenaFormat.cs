﻿namespace ArenaSoftware.ArenaCore.Filter
{
    /// <summary>
    /// Clase con información de localización. 
    /// </summary>
    public class ArenaFormat
    {
        /// <value>Locale.</value>
        public string Locale { get; set; } = "";

        /// <value>Formato de fecha.</value>
        public string DateFormat { get; set; } = "";

        /// <value>Zona de horario.</value>
        public string TimeZone { get; set; } = "";
    }
}
