﻿using ArenaSoftware.ArenaCore.Commons;
using ArenaSoftware.ArenaCore.DataAccess;
using ArenaSoftware.ArenaCore.Logs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using static ArenaSoftware.ArenaCore.DataAccess.Operator;

namespace ArenaSoftware.ArenaCore.Filter
{
    /// <summary>
    /// Clase constructora de listas de expresiones lógicas <c>WhereParameter</c> a partír de instancias <c>ArenaFilter</c>.
    /// </summary>
    public class FilterBuilder
    {
        /// <value>Logger.</value>
        protected ILogger Logger;

        /// <value>Generador de expresiones lógicas <c>QueryWhere</c>.</value>
        public QueryWhereBuilder QueryWhereBuilder { set; get; }

        /// <summary>
        /// Constructor con QueryWhereBuilder y logger. 
        /// </summary>
        /// <param name="queryWhereBuilder">Generador de expresiones lógicas <c>QueryWhere</c>.</param>
        /// <param name="logger">Logger.</param>
        public FilterBuilder(QueryWhereBuilder queryWhereBuilder, ILogger <FilterBuilder> logger)
        {
            this.QueryWhereBuilder = queryWhereBuilder;
            this.Logger = logger;
        }

        /// <summary>
        /// Método para construir un <c>QueryWhere</c> con un <c>ArenaFilter</c>. 
        /// </summary>
        /// <param name="arenaFilter">Filtro para aplicar.</param>
        /// <returns>
        /// Lista de <c>WhereParameter</c> con la expresión.
        /// </returns>
        public List<WhereParameter> Build(ArenaFilter arenaFilter)
        {
            return Build(arenaFilter, new string[] { });
        }

        /// <summary>
        /// Método para construir un <c>QueryWhere</c> con un <c>ArenaFilter</c>. 
        /// </summary>
        /// <param name="arenaFilter">Filtro para aplicar.</param>
        /// <param name="excludeFilters">Lista de campos filtro a excluir.</param>
        /// <returns>
        /// Lista de <c>WhereParameter</c> con la expresión.
        /// </returns>
        public List<WhereParameter> Build(ArenaFilter arenaFilter, string[] excludeFilters)
        {
            try
            {
                List<WhereParameter> properties = new List<WhereParameter>();

                foreach (ArenaFieldFilter fieldFilter in arenaFilter.FieldFilters)
                {
                    if (excludeFilters.Length > 0)
                    {
                        if (excludeFilters.Contains(fieldFilter.FieldName))
                        {
                            // Si es un campo a exluír, se evita su análisis.
                            this.Logger.LogDebug(LoggingEvents.FilterData, "Excluye [{0}]", fieldFilter.FieldName);
                            continue;
                        }
                    }

                    // Determina el operador.
                    OperatorEnum opFilter = OperatorEnum.NOTHING;
                    if (fieldFilter.Op != null)
                    {
                        opFilter = Operator.GetOperatorEnum(fieldFilter.Op);
                    }
                    if (fieldFilter.Values != null && fieldFilter.Values.Length > 0)
                    {
                        string fieldName = fieldFilter.FieldName;
                        string expresion = fieldFilter.Expression;

                        // Toma forceUppercase de ArenaFieldFilter.
                        // Si no se definió en la columna de filtro, se toma de arenaFilter, defecto false. 
                        bool forceUppercase = fieldFilter.ForceUppercase ?? arenaFilter.ForceUppercase ?? false;

                        // El tipo de dato del filtro.
                        string filterType = fieldFilter.DataType.ToLower();

                        // Operadores defecto.
                        if (opFilter == OperatorEnum.NOTHING)
                        {
                            if (fieldFilter.DataType.ToLower().Equals("string"))
                            {
                                // Para string es like.
                                opFilter = OperatorEnum.LIKE;
                            }
                            else
                            {
                                // Para todos los demás es EQUAL.
                                opFilter = OperatorEnum.EQUAL;
                            }
                        }

                        // Análisis por tipo.
                        if (filterType == "string")
                        {
                            // Operaciones sobre string.
                            string[] castValues = new string[fieldFilter.Values.Length];
                            int n = 0;
                            foreach (string value in fieldFilter.Values)
                            {
                                string castValue = value;
                                castValues[n] = castValue;
                                n++;
                            }

                            WhereParameter<string> property = new WhereParameter<string>(fieldName, opFilter, expresion, forceUppercase, castValues);
                            properties.Add(property);
                        }
                        else if (filterType == "date" || filterType == "datetime")
                        {
                            // Operaciones sobre date.
                            DateTime[] castValues = new DateTime[fieldFilter.Values.Length];
                            int n = 0;
                            foreach (string value in fieldFilter.Values)
                            {
                                string strValue = value.ToString();

                                // Le agrega el timezone.
                                strValue = strValue.Replace("Z", arenaFilter.TimeZone);

                                // Parsea la fecha con el formato.
                                var date = DateTime.ParseExact(strValue,
                                                            arenaFilter.DateFormat,
                                                            CultureInfo.InvariantCulture,
                                                            DateTimeStyles.AssumeUniversal |
                                                            DateTimeStyles.AdjustToUniversal);

                                if (fieldFilter.EnableDateWithTime)
                                {
                                    // Tiene el tiempo habilitado, deja como llegó.
                                    castValues[n] = date;
                                } else 
                                {
                                    // No usa tiempo, entonces evalúa truncando.
                                    castValues[n] = castValues[n].Date;
                                    string exp;
                                    if (string.IsNullOrEmpty(fieldFilter.Expression))
                                    {
                                        exp = fieldName;
                                    }
                                    else
                                    {
                                        exp = expresion;
                                    }
                                    // Crear función en el motor si no se encuentra:
                                    // TruncateTime. ejemplo MySql:
                                    //
                                    // Create FUNCTION TruncateTime(dateValue DateTime) RETURNS date
                                    // return Date(dateValue);
                                    //
                                    // ref https://stackoverflow.com/questions/19714022/canonical-function-entityfunctions-truncatetime-does-not-exist-in-mysql
                                    //
                                    expresion = "DbFunctions.TruncateTime(" + exp + ")";
                                }

                                n++;
                            }

                            WhereParameter<DateTime> property = new WhereParameter<DateTime>(fieldName, opFilter, expresion, forceUppercase, castValues);
                            properties.Add(property);
                        }
                        else if (filterType == "boolean")
                        {
                            // Operaciones sobre Boolean.
                            bool[] castValues = new Boolean[fieldFilter.Values.Length];
                            int n = 0;
                            foreach (object value in fieldFilter.Values)
                            {
                                string strValue = value.ToString();
                                bool castValue = bool.Parse(strValue);
                                castValues[n] = castValue;
                                n++;
                            }
                            WhereParameter<bool> property = new WhereParameter<bool>(fieldName, opFilter, expresion, forceUppercase, castValues);
                            properties.Add(property);
                        }
                        else if (filterType == "integer")
                        {
                            // Operaciones sobre integer.
                            int[] castValues = new int[fieldFilter.Values.Length];
                            int n = 0;
                            foreach (string value in fieldFilter.Values)
                            {
                                string strValue = value.ToString();
                                int castValue = int.Parse(strValue);
                                castValues[n] = castValue;
                                n++;
                            }
                            WhereParameter<int> property = new WhereParameter<int>(fieldName, opFilter, expresion, forceUppercase, castValues);
                            properties.Add(property);
                        }
                        else if (filterType == "long")
                        {
                            // Operaciones sobre long.
                            long[] castValues = new long[fieldFilter.Values.Length];
                            int n = 0;
                            foreach (string value in fieldFilter.Values)
                            {
                                string strValue = value.ToString();
                                long castValue = long.Parse(strValue);
                                castValues[n] = castValue;
                                n++;
                            }
                            WhereParameter<long> property = new WhereParameter<long>(fieldName, opFilter, expresion, forceUppercase, castValues);
                            properties.Add(property);
                        }
                        else if (filterType == "double")
                        {
                            // Operaciones sobre double.
                            CultureInfo culture;
                            if(!string.IsNullOrEmpty(arenaFilter.Locale))
                            {
                                culture = new CultureInfo(arenaFilter.Locale);
                            } else
                            {
                                culture = CultureInfo.CurrentCulture;
                            }

                            double[] castValues = new double[fieldFilter.Values.Length];
                            int n = 0;
                            foreach (string value in fieldFilter.Values)
                            {
                                string strValue = value.ToString();
                                double castValue = double.Parse(strValue, culture);
                                castValues[n] = castValue;
                                n++;
                            }
                            WhereParameter<double> property = new WhereParameter<double>(fieldName, opFilter, expresion, forceUppercase, castValues);
                            properties.Add(property);
                        }
                        else
                        {
                            throw new ArenaException("Unsupported type: " + fieldFilter.DataType);
                        }
                    }
                }

                this.Logger.LogDebug("Properties {0}", ArenaLogUtil.IEnumerableToString(properties));

                return properties;

            } catch(Exception e)
            {
                throw new ArenaException("Unsupported", e);
            }

        }
    }
}
