﻿using System;
using System.Collections.Generic;

namespace ArenaSoftware.ArenaCore.Filter
{
    /// <summary>
    /// Clase con información de un filtro lógico estructurado. 
    /// </summary>
    /// <remarks>
    /// Generalmente se utiliza desde una API Web y desde los servicios genéricos.
    /// </remarks>
    public class ArenaFilter : ArenaFormat
    {
        /// <value>Lista de campos filtrados.</value>
        public List<ArenaFieldFilter> FieldFilters { get; set; } = new List<ArenaFieldFilter>();

        /// <value>Expresión WHERE.</value>
        public string Where { get; set; } = "";

        /// <value>Expresión ORDER BY.</value>
        public string OrderBy { get; set; } = "";

        /// <value>Indica si se debe forzar a Uppercase.</value>
        public Nullable<bool> ForceUppercase { get; set; }

        /// <value>Nodos que se deben incluir.</value>
        public string[] ExtraNodes { get; set; } = new string[] { };
    }
}
