using System.ComponentModel.DataAnnotations.Schema;

namespace ArenaSoftware.ArenaCore.Models
{
    /// <summary>
    /// Clase con informaci�n para �tems de colecci�n manejados desde la API Web. 
    /// </summary>
    /// <remarks>
    /// Las propiedades "ToDelete", "NewItem" e "Index" se utilizan para almacenar informaci�n enviada desde un cliente de API Web.
    /// </remarks>
    public class ArenaCollectionItem
    {
        /// <value>Indica si el �tem est� marcado para eliminar.</value>
        [NotMapped]
        public bool ToDelete { get; set; } = false;

        /// <value>Indica si el �tem est� marcado como nuevo.</value>
        [NotMapped]
        public bool NewItem { get; set; } = false;

        /// <value>Indica el sub�ndice del �tem dentro de la colecci�n.</value>
        [NotMapped]
        public int Index { get; set; }
    }
}