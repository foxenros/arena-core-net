﻿using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArenaSoftware.ArenaCore.Email
{
    /// <summary>
    /// Clase con información de direcciones de correo electrónico. 
    /// </summary>
    public class EmailAddress
    {
        /// <summary>
        /// Constructor con nombre y dirección. 
        /// </summary>
        /// <param name="name">Nombre.</param>
        /// <param name="address">Dirección.</param>
        public EmailAddress(string name, string address)
        {
            Name = name;
            Address = address;
        }

        /// <value>Nombre.</value>
        public string Name { get; set; }

        /// <value>Dirección.</value>
        public string Address { get; set; }
    }

    /// <summary>
    /// Clase con información sobre mensaje de correo electrónico. 
    /// </summary>
    public class EmailMessage
    {
        /// <summary>
        /// Constructor sin parámetros. 
        /// </summary>
        public EmailMessage()
        {
            ToAddresses = new List<EmailAddress>();
            FromAddresses = new List<EmailAddress>();
        }

        /// <value>Dirigido a.</value>
        public List<EmailAddress> ToAddresses { get; set; }

        /// <value>Desde dirección.</value>
        public List<EmailAddress> FromAddresses { get; set; }
        
        /// <value>Tema.</value>
        public string Subject { get; set; }

        /// <value>Contenido.</value>
        public string Content { get; set; }
    }

    /// <summary>
    /// Iterfaz de servicio de email. 
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Envío de correo asincrónico.
        /// </summary>
        /// <param name="emailMessage">Mensaje.</param>
        Task<Boolean> SendAsync(EmailMessage emailMessage);
    }

    /// <summary>
    /// Implementación de servicio de email. 
    /// </summary>
    public class EmailService : IEmailService
    {
        private readonly EmailConfiguration _emailConfiguration;

        /// <summary>
        /// Constructor con configuración. 
        /// </summary>
        /// <param name="emailConfiguration">Configuración.</param>
        public EmailService(EmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }

        /// <summary>
        /// Envío de correo asincrónico.
        /// </summary>
        /// <param name="emailMessage">Mensaje.</param>
        public async Task<Boolean> SendAsync(EmailMessage emailMessage)
        {
            var message = new MimeMessage();
            message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

            message.Subject = emailMessage.Subject;
            // En formato HTML.
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };

            using (var emailClient = new SmtpClient())
            {
                try
                {
                    // TODO: Configurado que no usa ssl, ver parámetro.
                    await emailClient.ConnectAsync(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, false);

                    // Quita "XOAUTH2". 
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    await emailClient.AuthenticateAsync(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                    await emailClient.SendAsync(message);
                    await emailClient.DisconnectAsync(true);

                    return true;

                }
                catch (Exception e)
                {
                    this.Logger.LogDebug("Se crea perfil de [{0}] en el cache", e);
                    return false;
                }

            }
        }
    }
}
