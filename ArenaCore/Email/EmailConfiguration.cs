﻿namespace ArenaSoftware.ArenaCore.Email
{
    /// <summary>
    /// Clase con información sobre la configuración de email. 
    /// </summary>
    public class EmailConfiguration
    {
        /// <value>Servidor smtp.</value>
        public string SmtpServer { get; set; }

        /// <value>Puerto smtp.</value>
        public int SmtpPort { get; set; }

        /// <value>Usuario smtp.</value>
        public string SmtpUsername { get; set; }

        /// <value>Contraseña smtp.</value>
        public string SmtpPassword { get; set; }

        /// <value>Servidor pop.</value>
        public string PopServer { get; set; }

        /// <value>Puerto pop.</value>
        public int PopPort { get; set; }

        /// <value>Usuario pop.</value>
        public string PopUsername { get; set; }

        /// <value>Contraseña pop.</value>
        public string PopPassword { get; set; }
    }
}
