﻿using ArenaSoftware.ArenaCore.Commons;
using ArenaSoftware.ArenaCore.Filter;
using ArenaSoftware.ArenaCore.Logs;
using ArenaSoftware.ArenaCore.Security;
using ArenaSoftware.ArenaCore.Service;
using ArenaSoftware.ArenaCore.Services;
using ArenaSoftware.ArenaCore.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaSoftware.ArenaCore.Controllers
{
    /// <summary>
    /// Clase genérica de controllers WorkWith (trabajar con). 
    /// Implementa tareas comunes REST como GET, PUT, POST y otras más que son comunes a los WorkWith.
    /// </summary>
    /// <typeparam name="E">Tipo de los ítems.</typeparam>
    /// <typeparam name="S">Tipo de la clase menejadora del servicio.</typeparam>
    /// <typeparam name="K">Tipo para el ID. Se debe indicar sólo para clave simple. En caso de claves compuestas indicar <c>object</c>.</typeparam>
    public class ArenaWorkWithController<E, S, K> : Controller where E : class
    {
        /// <value>Servicio para revisar los permisos de seguridad.</value>
        protected IPermissionChecker PermissionChecker { get; set; }

        /// <value>Servicio genérico manejador de los ítems.</value>
        protected ArenaWorkWithService<E, K> Service { get; set; }

        /// <value>Código de ítem de menú asociado al <c>Controller</c>.</value>
        protected string MenuItId { get; set; }

        /// <value>Logger.</value>
        protected ILogger Logger;

        /// <summary>
        /// Constructor sin parámetros. 
        /// </summary>
        /// 
        public ArenaWorkWithController()
        {
        }

        /// <summary>
        /// Constructor con servicio, manejador de permisos y logger. 
        /// </summary>
        /// <param name="service">Servicio manejador de los ítems.</param>
        /// <param name="permissionChecker">Manejador de permisos de seguridad.</param>
        /// <param name="logger">Logger.</param>
        public ArenaWorkWithController(ArenaWorkWithService<E, K> service, IPermissionChecker permissionChecker, ILogger logger)
        {
            Init(service, permissionChecker, logger);
        }

        /// <summary>
        /// Iniciador que debe ser llamado únicamente cuando NO se crea la instancia desde el contructor con parámetros.
        /// </summary>
        /// <param name="service">Servicio manejador de los ítems.</param>
        /// <param name="permissionChecker">Manejador de permisos de seguridad.</param>
        /// <param name="logger">Logger.</param>
        public void Init(ArenaWorkWithService<E, K> service, IPermissionChecker permissionChecker, ILogger logger)
        {
            this.Service = service;
            this.PermissionChecker = permissionChecker;
            Init();
        }

        /// <summary>
        /// Iniciador que debe ser llamado únicamente cuando NO se crea la instancia desde el contructor con parámetros.
        /// </summary>
        public void Init()
        {
            SetFromAttributes();
        }

        private void SetFromAttributes()
        {
            var attribute = this.GetType().GetCustomAttributes(
            typeof(ArenaAuthorizationAttribute), true).FirstOrDefault() as ArenaAuthorizationAttribute;
            if (attribute != null)
            {
                this.MenuItId = attribute.MenuItId;
            }
        }

        /// <summary>
        /// Obtiene "NotFound" en caso cuando no se envía parámetro de Id y evitando un error 500.
        /// </summary>
        /// <returns>
        /// NotFound().
        /// </returns>
        [HttpGet()]
        [Authorize(Policy = ArenaAuthorizationRequirement.PolicyName)]
        [ArenaAuthorizationAction(accessCode: ActionWorkWith.GetAccessCode)]
        public virtual IActionResult GetNotFound() {
            return NotFound();
        }

        /// <summary>
        /// Obtiene un ítem desde el menejador del servicio a traves de <c>GetItemEager</c>.
        /// </summary>
        /// <param name="id">ID del ítem.</param>
        /// <returns>
        /// Ítem desde el menejador del servicio.
        /// </returns>
        [HttpGet("{id}")]
        [Authorize(Policy = ArenaAuthorizationRequirement.PolicyName)]
        [ArenaAuthorizationAction(accessCode: ActionWorkWith.GetAccessCode)]
        public virtual IActionResult GetItemEager(K id)
        {
            try
            {
                E item = Service.GetItemEager(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Json(item);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Obtiene un ítem desde el menejador del servicio a traves de <c>GetItem</c>.
        /// </summary>
        /// <remarks>
        /// Generalmente se devuelve el ítem sin los objetos FK relacionados.
        /// </remarks>
        /// <param name="id">ID del ítem.</param>
        /// <returns>
        /// Ítem desde el menejador del servicio.
        /// </returns>
        [HttpGet("flat/{id}")]
        [Authorize(Policy = ArenaAuthorizationRequirement.PolicyName)]
        [ArenaAuthorizationAction(accessCode: ActionWorkWith.GetAccessCode)]
        public virtual IActionResult GetItem(K id)
        {
            try
            {
                E item = Service.GetItem(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Json(item);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Método protegido para llamar desde las clases que heredan.
        /// </summary>
        /// <remarks>
        /// Generalmente se utiliza en caso de sobreescribir <c>GetItem</c>.
        /// </remarks>
        /// <param name="values">Valores de la clave.</param>
        /// <returns>
        /// <c>IActionResult</c> generado.
        /// </returns>
        protected IActionResult CallGetItemEager(object[] values)
        {
            try
            {
                E item = Service.GetItemEager(values);
                if (item == null)
                {
                    return NotFound();
                }
                return Json(item);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Método protegido para llamar desde las clases que heredan.
        /// </summary>
        /// <remarks>
        /// Generalmente se utiliza en caso de sobreescribir <c>GetItem</c>.
        /// </remarks>
        /// <param name="values">Valores de la clave.</param>
        /// <returns>
        /// <c>IActionResult</c> generado.
        /// </returns>
        protected IActionResult CallGetItem(object[] values)
        {
            try
            {
                E item = Service.GetItem(values);
                if (item == null)
                {
                    return NotFound();
                }
                return Json(item);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Obtiene una lista de ítems desde el menejador del servicio a traves de <c>GetList</c>.
        /// </summary>
        /// <param name="arenaFilter">Filtro para aplicar.</param>
        /// <returns>
        /// Lista de ítems desde el menejador del servicio.
        /// </returns>
        /// <exception cref="ArenaException">Cuando no se pueden recuperar los datos.</exception>
        [HttpPost("list")]
        [Authorize(Policy = ArenaAuthorizationRequirement.PolicyName)]
        [ArenaAuthorizationAction(accessCode: ActionWorkWith.ListAccessCode)]
        public virtual IActionResult GetList([FromBody] ArenaFilter arenaFilter)
        {
            try
            {
                List<E> list = Service.GetList(arenaFilter);
                return Json(list);
            }
            catch (Exception e)
            {
                this.Logger.LogDebug(LoggingEvents.FilterData, "No se pudieron recuperar los datos: [{0}]", arenaFilter);
                throw new ArenaException("Fallo al recuperar los datos: " + arenaFilter, e);
            }
        }

        /// <summary>
        /// Realiza una validación mediante la información de <c>ValidationInfo</c>.
        /// </summary>
        /// <param name="validationInfo">Información para realizar la validación.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        [HttpPost("valid")]
        public IActionResult Valid([FromBody] ValidationInfo validationInfo)
        {
            try
            {
                // Completa el Assembly.
                validationInfo.Assembly = this.GetType().Assembly;

                var avr = Service.Validate(validationInfo);
                return Json(avr);
            }
            catch (Exception e)
            {
                this.Logger.LogDebug(LoggingEvents.Validation, "No se pudo validar: [{0}]", validationInfo);
                throw e;
            }
        }

        /// <summary>
        /// Actualiza un ítem por medio del manejador del servicio.
        /// </summary>
        /// <param name="saveParams">Información del ítem a actualizar.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        /// <exception cref="ArenaSaveException">Cuando no se puede actualizar el ítem.</exception>
        [HttpPut("")]
        [Authorize(Policy = ArenaAuthorizationRequirement.PolicyName)]
        [ArenaAuthorizationAction(accessCode: ActionWorkWith.UpdateAccessCode)]
        public virtual IActionResult Update([FromBody] SaveParams<E> saveParams)
        {
            try
            {
                ArenaValidationResponse<E> avr = Service.Save(saveParams.Item, saveParams.Action);
                return Json(avr);
            }
            catch (Exception e)
            {
                this.Logger.LogDebug(LoggingEvents.WorkWithController, "No se pudo actualizar: [{0}]", saveParams);
                throw new ArenaSaveException("No se pudo actualizar: " + saveParams.Item, e);
            }
        }

        /// <summary>
        /// Inserta un ítem por medio del manejador del servicio.
        /// </summary>
        /// <param name="saveParams">Información del ítem a insertar.</param>
        /// <returns>
        /// Respuesta con posibles errores y/o advertencias.
        /// </returns>
        /// <exception cref="ArenaSaveException">Cuando no se puede insertar el ítem.</exception>
        [HttpPost("")]
        [Authorize(Policy = ArenaAuthorizationRequirement.PolicyName)]
        [ArenaAuthorizationAction(accessCode: ActionWorkWith.InsertAccessCode)]
        public virtual IActionResult Insert([FromBody] SaveParams<E> saveParams)
        {
            try
            {
                ArenaValidationResponse<E> avr = Service.Save(saveParams.Item, ActionWorkWithEnum.INSERT);
                return Json(avr);
            }
            catch (Exception e)
            {
                this.Logger.LogDebug(LoggingEvents.WorkWithController, "No se pudo insertar: [{0}]", saveParams);
                throw new ArenaSaveException("No se pudo insertar: " + saveParams.Item, e);
            }
        }

    }
}
