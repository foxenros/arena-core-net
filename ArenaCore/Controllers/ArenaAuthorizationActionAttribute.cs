﻿using System;

namespace ArenaSoftware.ArenaCore.Controllers
{
    /// <summary>
    /// Clase de atributos con información para métodos de <c>Controllers</c>. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ArenaAuthorizationActionAttribute : Attribute 
    {
        /// <summary>
        /// Constructor con código de acceso. 
        /// </summary>
        /// <param name="accessCode">Código de acceso.</param>
        public ArenaAuthorizationActionAttribute(string accessCode)
        {
            this.AccessCode = accessCode;
        }

        /// <value>Código de acceso.</value>
        public string AccessCode { get; set; }
    }
}
