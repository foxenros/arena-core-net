﻿using System;

namespace ArenaSoftware.ArenaCore.Controllers
{
    /// <summary>
    /// Clase de atributos con información para clases <c>Controllers</c>. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ArenaAuthorizationAttribute : Attribute
    {
        /// <summary>
        /// Constructor con código de ítem de menú. 
        /// </summary>
        /// <param name="menuItId">Código de ítem de menú.</param>
        public ArenaAuthorizationAttribute(string menuItId)
        {
            this.MenuItId = menuItId;
        }

        /// <value>Código de ítem de menú.</value>
        public string MenuItId { get; set; }
    }
}
