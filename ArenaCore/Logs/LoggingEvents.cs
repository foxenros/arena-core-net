﻿namespace ArenaSoftware.ArenaCore.Logs
{
    /// <summary>
    /// Eventos de logging.
    /// </summary>
    public class LoggingEvents
    {
        /// <value>Seguridad.</value>
        public const int Security = 1000;

        /// <value>Servicios WorkWith.</value>
        public const int WorkWithService = 2000;

        /// <value>Controladores WorkWith.</value>
        public const int WorkWithController = 3000;

        /// <value>Email.</value>
        public const int Email = 4000;

        /// <value>Filtros y expresiones.</value>
        public const int FilterData = 5000;

        /// <value>Validación.</value>
        public const int Validation = 6000;

        /// <value>Aplicación.</value>
        public const int Application = 7000;
    }
}
