﻿using System;
using System.Collections;
using System.Text;

namespace ArenaSoftware.ArenaCore.Logs
{
    /// <summary>
    /// Clase útil para soporte de logger.
    /// </summary>
    public class ArenaLogUtil
    {
        /// <summary>
        /// Devuelve una cadena con el resultado de iterar el IEnumerable.
        /// </summary>
        /// <returns>
        /// Cadena para logger.
        /// </returns>
        public static String IEnumerableToString(IEnumerable iEnumerable)
        {
            try
            {
                StringBuilder ret = new StringBuilder();
                if (iEnumerable != null)
                {
                    foreach (Object field in iEnumerable)
                    {
                        if (ret.Length != 0)
                        {
                            ret.Append(", ");
                        }
                        ret.Append(field);
                    }
                }
                return "[" + ret.ToString() + "]";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
