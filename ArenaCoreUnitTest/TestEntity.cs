﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArenaCoreUnitTest
{
    public class TestEntity
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return "TestEntity=[" +
                "Id=" + Id + ", " +
                "Description=" + Description;
        }
    }
}
