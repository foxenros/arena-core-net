﻿using ArenaSoftware.ArenaCore.DataAccess;
using ArenaSoftware.ArenaCore.Email;
using ArenaSoftware.ArenaCore.Filter;
using ArenaSoftware.ArenaCore.Security;
using ArenaSoftware.ArenaCore.Validation;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ArenaCoreUnitTest
{
    public class TestStartup    
    {
        /// <value>Configuration.</value>
        public IConfiguration Configuration { get; }

        public TestStartup(IHostingEnvironment env, IServiceProvider serviceProvider, IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureTestServices(IServiceCollection services)
        {
            // Agrega el soporte para consultas.
            services.AddSingleton<QueryWhereBuilder>();

            // Agrega el creador de filtros.
            services.AddSingleton<FilterBuilder>();

            // Agrega el servicio de perfil (con accesos "testitem1"="total", "testitem2"="parcial" y "testitem3"="denegado").
            // Para todos los usuarios se usa el mismo perfil y el usuario "admin" devuelve además IsAdmin=true. 
            services.AddSingleton<IProfileUtil, TestProfileUtil>();

            // Agrega el servicio de chequeo de permisos.
            services.AddSingleton<IPermissionChecker, PermissionChecker>();

            // Agrega el servicio de token.
            services.AddSingleton<ITokenBuilder, TokenBuilder>();

            // Agrega el servicio de configuración de correo.
            services.AddSingleton<EmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());

            // Agrega el servicio de correo.
            services.AddSingleton<IEmailService, EmailService>();

            // Agrega el servicio de ayuda de validación.
            services.AddSingleton<ArenaValidationUtil>();

            var connection = new SqliteConnection("DataSource=:memory:");
            SQLitePCL.raw.SetProvider(new SQLitePCL.SQLite3Provider_e_sqlite3());
            connection.Open();

            // Agrega el contexto de base de datos.
            services.AddDbContext<TestDbContext>(options => options.UseSqlite(connection));

            // Agrega el servicio de ayuda de validación.
            services.AddSingleton<TestService>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            TelemetryConfiguration.Active.DisableTelemetry = true;
        }
    }
}
