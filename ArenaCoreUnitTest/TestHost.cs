﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.IO;
namespace ArenaCoreUnitTest
{
    public class TestHost
    {
        public TestServer Server { set; get;}

        public TestHost()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                            .AddEnvironmentVariables();

            var configuration = builder.Build();

            var host = new WebHostBuilder()
                .UseEnvironment("Test")
                .UseConfiguration(configuration)
                .ConfigureLogging((logging) =>
                {
                    logging.AddConsole();
                    logging.AddDebug();
                    logging.AddConfiguration(configuration.GetSection("Logging"));

                })
                .UseStartup<TestStartup>();

            this.Server = new TestServer(host);
        }
    }
}
