﻿using ArenaSoftware.ArenaCore.Filter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaCoreUnitTest
{
    [TestClass]
    public class ArenaWorkWithServiceTest
    {
        private TestDbContext context;
        private TestService service;

        public ArenaWorkWithServiceTest()
        {
            var server = new TestHost().Server;
            context = (TestDbContext) server.Host.Services.GetService(typeof(TestDbContext));
            service = (TestService) server.Host.Services.GetService(typeof(TestService));
        }

        [TestMethod]
        public void BasicTest()
        {
            // Asegura la base de datos.
            context.Database.EnsureCreated();

            // Transacción.
            //using (var trans = context.Database.BeginTransaction())
            //{
                // Entrada 1.
                var entry1 = new TestEntity()
                {
                    Id = 1,
                    Description = "Test1"
                };

                // Entrada 2.
                var entry2 = new TestEntity()
                {
                    Id = 2,
                    Description = "Test2"
                };

                // Inserta entrada 1 y 2.
                var insertResult1 = service.Save(entry1, ActionWorkWithEnum.INSERT);
                Assert.IsFalse(insertResult1.HasErrors());
                var insertResult2 = service.Save(entry2, ActionWorkWithEnum.INSERT);
                Assert.IsFalse(insertResult2.HasErrors());

                // Actualiza entrada 1.
                var entryGet = service.GetItem(1);
                entryGet.Description = "Test 1 actualizado";
                var updateResult = service.Save(entryGet, ActionWorkWithEnum.UPDATE);
                Assert.IsFalse(updateResult.HasErrors());

                //trans.Commit();
            //}

            // Consulta 1 (por filter).
            var list = service.GetList(new ArenaFilter()
            {
                FieldFilters = {
                    new ArenaFieldFilter()
                    {
                        FieldName = "Id",
                        DataType = "integer",
                        Op = "=",
                        Values = new string[]{ "1" }
                    }
                }
            });

            Assert.IsNotNull(list);
            Assert.AreEqual("Test 1 actualizado", list[0].Description);
        }

    }
}
