using ArenaSoftware.ArenaCore.Commons;
using ArenaSoftware.ArenaCore.Filter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static ArenaSoftware.ArenaCore.DataAccess.Operator;

namespace ArenaCoreUnitTest
{
    [TestClass]
    public class FilterBuilderTest
    {
        private FilterBuilder filterBuilder;

        public FilterBuilderTest()
        {
            var server = new TestHost().Server;
            filterBuilder = (FilterBuilder) server.Host.Services.GetService(typeof(FilterBuilder));
        }

        [TestMethod]
        public void TruncateDateTimeTest()
        {
            ArenaFilter filter = new ArenaFilter();
            filter.DateFormat = "dd/MM/yyyy";

            ArenaFieldFilter field = new ArenaFieldFilter();
            field.FieldName = "fecha";
            field.Op = "=";
            field.DataType = "datetime";
            field.Values = new string[] { "01/03/2018" };
            field.EnableDateWithTime = false;

            filter.FieldFilters.Add(field);

            var parameters = filterBuilder.Build(filter);

            // Debe truncar la evaluaci�n porque "EnableDateWithTime = false".
            Assert.AreEqual("DbFunctions.TruncateTime(" + field.FieldName + ")", parameters[0].Expression);

            // Luego se prueba con "EnableDateWithTime = true".
            field.EnableDateWithTime = true;
            parameters = filterBuilder.Build(filter);

            // No debe truncar la evaluaci�n porque "EnableDateWithTime" es verdadero. Expression debe ser "".
            Assert.AreEqual(parameters[0].Expression, "");
        }

        [TestMethod]
        public void LocaleDoubleParseTest()
        {
            ArenaFilter filter = new ArenaFilter();
            filter.Locale = "es-ar";

            ArenaFieldFilter field = new ArenaFieldFilter();
            field.FieldName = "valor";
            field.Op = "=";
            field.DataType = "double";
            field.Values = new string[] { "10,5" };

            filter.FieldFilters.Add(field);

            var parameters = filterBuilder.Build(filter);
            // Debe reconocer el n�mero 10,5 como [10.5].
            Assert.AreEqual(parameters[0].ValuesUntyped[0], 10.5);

            // Ahora se prueba con separador "." que NO deber�a interpretarse igual a "coma decimal".
            field.Values = new string[] { "10.5" };
            parameters = filterBuilder.Build(filter);

            // NO debe reconocer el n�mero 10,5 como [10.5].
            Assert.AreNotEqual(parameters[0].ValuesUntyped[0], 10.5);
        }

        [TestMethod]
        public void ExcludeFilterTest()
        {

            ArenaFilter filter = new ArenaFilter();
            filter.Locale = "es-ar";

            ArenaFieldFilter field1 = new ArenaFieldFilter();
            field1.FieldName = "valor";
            field1.Op = "=";
            field1.DataType = "double";
            field1.Values = new string[] { "10.5" };
            filter.FieldFilters.Add(field1);

            ArenaFieldFilter field2 = new ArenaFieldFilter();
            field2.FieldName = "valor2";
            field2.Op = "=";
            field2.DataType = "double";
            field2.Values = new string[] { "34,98" };
            filter.FieldFilters.Add(field2);

            var parameters = filterBuilder.Build(filter, new string[] { "valor2" });

            // Debe excluisrse el par�metro "valor2" de los filtros.
            Assert.AreEqual(parameters.Count, 1);
        }

        [TestMethod]
        public void UnsoportedTypeTest()
        {
            ArenaFilter filter = new ArenaFilter();
            filter.Locale = "es-ar";

            ArenaFieldFilter field1 = new ArenaFieldFilter();
            field1.FieldName = "valor";
            field1.Op = "=";
            field1.DataType = "tipo-inexistente";
            field1.Values = new string[] { "ZZZZ" };
            filter.FieldFilters.Add(field1);

            try
            {
                var parameters = filterBuilder.Build(filter);

                // Si llega por aqu� es que algo sali� mal, se muestra el test como "error".
                Assert.IsTrue(false);
            }
            catch (ArenaException e)
            {
                // Es correcta la excepci�n, el tipo "tipo-inexistente" NO debe ser reconocido.
                Assert.IsNotNull(e);
            }
        }

        [TestMethod]
        public void OperadoresDefectoTest()
        {
            ArenaFilter filter = new ArenaFilter();
            filter.Locale = "es-ar";

            ArenaFieldFilter field1 = new ArenaFieldFilter();
            field1.FieldName = "valor";
            field1.Op = null;
            field1.DataType = "double";
            field1.Values = new string[] { "10,5" };

            filter.FieldFilters.Add(field1);

            ArenaFieldFilter field2 = new ArenaFieldFilter();
            field2.FieldName = "valor";
            field2.Op = null;
            field2.DataType = "string";
            field2.Values = new string[] { "valor" };

            filter.FieldFilters.Add(field2);

            var parameters = filterBuilder.Build(filter);
            // Debe ser EQUAL para los que no sean string.
            Assert.AreEqual(OperatorEnum.EQUAL, parameters[0].Op);
            // Debe ser LIKE para string.
            Assert.AreEqual(OperatorEnum.LIKE, parameters[1].Op);
        }

    }
}
