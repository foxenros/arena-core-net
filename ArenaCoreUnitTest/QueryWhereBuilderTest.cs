using ArenaSoftware.ArenaCore.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using static ArenaSoftware.ArenaCore.DataAccess.Operator;

namespace ArenaCoreUnitTest
{
    [TestClass]
    public class QueryWhereBuilderTest
    {
        private QueryWhereBuilder queryWhereBuilder;

        public QueryWhereBuilderTest()
        {
            var server = new TestHost().Server;
            queryWhereBuilder = (QueryWhereBuilder) server.Host.Services.GetService(typeof(QueryWhereBuilder));
        }

        [TestMethod]
        public void OperatorsTest()
        {
            List<WhereParameter> parameters = new List<WhereParameter>
            {
                new WhereParameter<string>("campo", OperatorEnum.EQUAL, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.NOTEQUAL, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.GREATERTHAN, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.LESSTHAN, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.GREATERTHAN_OR_EQUALTO, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.LESSTHAN_OR_EQUALTO, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.IN, new string[] { "'valor1', 'valor2'" }),
                new WhereParameter<string>("campo", OperatorEnum.LIST, new string[] { "valor1", "valor2" }),
                new WhereParameter<string>("campo", OperatorEnum.NOT_LIST, new string[] { "valor1", "valor2" }),
                new WhereParameter<string>("campo", OperatorEnum.LIKE, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.START_WITH, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.END_WITH, new string[] { "valor" }),
                new WhereParameter<string>("campo", OperatorEnum.BETWEEN, new string[] { "valor1", "valor2" }),
                new WhereParameter<string>("campo", OperatorEnum.NOT_BETWEEN, new string[] { "valor1", "valor2" }),
                new WhereParameter<string>("campo", OperatorEnum.ISNULL),
                new WhereParameter<string>("campo", OperatorEnum.ISNOTNULL)
            };

            var queryWhere = queryWhereBuilder.Build(parameters);
            Assert.IsNotNull(queryWhere.Where);
            // Se espera la expresión indicada.
            string expected = "campo = @0 && campo <> @1 && campo > @2 && campo < @3 && campo >= @4 && campo <= @5 && campo IN ('valor1', 'valor2') && campo IN (@6,@7) && campo NOT IN (@8,@9) && campo.Contains(@10) && campo.StartsWith(@11) && campo.EndsWith(@12) && campo BETWEEN @13 AND @14 && campo NOT BETWEEN @15 AND @16 && campo IS NULL && NOT campo IS NULL";
            Assert.AreEqual(expected, queryWhere.Where);
            Assert.AreEqual(17, queryWhere.Parameters.Count);
        }

        [TestMethod]
        public void UppercaseTest()
        {
            List<WhereParameter> parameters = new List<WhereParameter>
            {
                new WhereParameter<string>("campo1", OperatorEnum.EQUAL, "", true, new string[] { "valor" }),
                new WhereParameter<string>("campo2", OperatorEnum.EQUAL, "left(campo2, 5)", true, new string[] { "valor" })
            };

            var queryWhere = queryWhereBuilder.Build(parameters);
            Assert.IsNotNull(queryWhere.Where);

            // TODO: verificar si es correcto para LINQ.
            string expected = "campo1.ToUpper() = @0 && left(campo2, 5).ToUpper() = @1";
            Assert.AreEqual(expected, queryWhere.Where);
            Assert.AreEqual("VALOR", queryWhere.Parameters[0]);
            Assert.AreEqual("VALOR", queryWhere.Parameters[1]);
        }

    }
}
