﻿using ArenaSoftware.ArenaCore.Email;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace ArenaCoreUnitTest
{
    [TestClass]
    public class EmailServiceTest
    {

        private IEmailService emailService;

        public EmailServiceTest()
        {
            var server = new TestHost().Server;
            emailService = (IEmailService) server.Host.Services.GetService(typeof(IEmailService));
        }

        [TestMethod]
        public void SendAsyncTest()
        {
            EmailMessage emailMessage = new EmailMessage();

            emailMessage.ToAddresses.Add(new EmailAddress("test", "test@arenasoftware.com.ar"));
            emailMessage.FromAddresses.Add(new EmailAddress("test", "test@arenasoftware.com.ar"));
            emailMessage.Subject = "Asunto de prueba";
            emailMessage.Content = "Contenido de prueba";

            Task<Boolean> task = emailService.SendAsync(emailMessage);
            Assert.IsTrue(task.Result);
        }
    }
}
