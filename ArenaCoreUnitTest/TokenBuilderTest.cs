﻿using ArenaSoftware.ArenaCore.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

namespace ArenaCoreUnitTest
{
    [TestClass]
    public class TokenBuilderTest
    {

        private ITokenBuilder tokenBuilder;

        public TokenBuilderTest()
        {
            var server = new TestHost().Server;
            tokenBuilder = (ITokenBuilder) server.Host.Services.GetService(typeof(ITokenBuilder));
        }

        [TestMethod]
        public void DataFromTokenTest()
        {
            // Autentica con "user".
            BasicAuthenticationIdentity identity = new BasicAuthenticationIdentity("user", "contraseña", "empresa", "origen");
            string token = tokenBuilder.CreateToken(identity);

            // Se espera extraiga el usuario "user".
            Assert.AreEqual("user", tokenBuilder.UsernameFromToken(token));

            // Se espera extraiga la empresa "empresa".
            Assert.AreEqual("empresa", tokenBuilder.CompanyFromToken(token));

            // Se espera extraiga el origen "origen".
            Assert.AreEqual("origen", tokenBuilder.OriginFromToken(token));

        }

        [TestMethod]
        public void HashTest()
        {
            // Se espera se haga un hash diferente a la cadena origen.
            Assert.AreNotEqual("test", tokenBuilder.HashMD5("test"));
        }

        [TestMethod]
        public void ExpireTest()
        {

            // Se configura la expiración en "1" minuto.
            tokenBuilder.TokenMinutesExpire = 1;

            // Autentica con "user".
            BasicAuthenticationIdentity identity = new BasicAuthenticationIdentity("user", "contraseña", "empresa", "origen");
            string token = tokenBuilder.CreateToken(identity);

            // Se esera un segundo.
            Thread.Sleep(1000);

            // Se espera que el token sea aceptado (no pasó un minuto).
            Assert.IsTrue(tokenBuilder.ValidateToken(token, identity));

            // Se esera un minuto
            Thread.Sleep(60000);

            // Se espera que el token sea rechazado.
            Assert.IsFalse(tokenBuilder.ValidateToken(token, identity));
        }

    }
}
