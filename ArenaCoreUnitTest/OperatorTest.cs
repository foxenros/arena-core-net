﻿using ArenaSoftware.ArenaCore.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static ArenaSoftware.ArenaCore.DataAccess.Operator;

namespace ArenaCoreUnitTest
{
    [TestClass]
    public class OperatorTest
    {

        [TestMethod]
        public void GetOperatorsCountTest()
        {
            // Se prueba la cantidad de operadores de cada operador.
            Assert.AreEqual(0, Operator.GetOperatorsCount(Operator.OperatorEnum.NOTHING));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.EQUAL));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.NOTEQUAL));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.GREATERTHAN));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.LESSTHAN));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.GREATERTHAN_OR_EQUALTO));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.LESSTHAN_OR_EQUALTO));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.IN));
            Assert.AreEqual(-1, Operator.GetOperatorsCount(Operator.OperatorEnum.LIST));
            Assert.AreEqual(-1, Operator.GetOperatorsCount(Operator.OperatorEnum.NOT_LIST));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.LIKE));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.START_WITH));
            Assert.AreEqual(1, Operator.GetOperatorsCount(Operator.OperatorEnum.END_WITH));
            Assert.AreEqual(2, Operator.GetOperatorsCount(Operator.OperatorEnum.BETWEEN));
            Assert.AreEqual(2, Operator.GetOperatorsCount(Operator.OperatorEnum.NOT_BETWEEN));
            Assert.AreEqual(0, Operator.GetOperatorsCount(Operator.OperatorEnum.ISNULL));
            Assert.AreEqual(0, Operator.GetOperatorsCount(Operator.OperatorEnum.ISNOTNULL));
        }

        [TestMethod]
        public void GetOperatorTest()
        {
            // Se comprueba el enum asociado a cada operador de texto resumido.
            Assert.AreEqual("", Operator.GetOperator(Operator.OperatorEnum.NOTHING));
            Assert.AreEqual("=", Operator.GetOperator(Operator.OperatorEnum.EQUAL));
            Assert.AreEqual("<>", Operator.GetOperator(Operator.OperatorEnum.NOTEQUAL));
            Assert.AreEqual(">", Operator.GetOperator(Operator.OperatorEnum.GREATERTHAN));
            Assert.AreEqual("<", Operator.GetOperator(Operator.OperatorEnum.LESSTHAN));
            Assert.AreEqual(">=", Operator.GetOperator(Operator.OperatorEnum.GREATERTHAN_OR_EQUALTO));
            Assert.AreEqual("<=", Operator.GetOperator(Operator.OperatorEnum.LESSTHAN_OR_EQUALTO));
            Assert.AreEqual("IN", Operator.GetOperator(Operator.OperatorEnum.IN));
            Assert.AreEqual("LIST", Operator.GetOperator(Operator.OperatorEnum.LIST));
            Assert.AreEqual("NOT_LIST", Operator.GetOperator(Operator.OperatorEnum.NOT_LIST));
            Assert.AreEqual("LIKE", Operator.GetOperator(Operator.OperatorEnum.LIKE));
            Assert.AreEqual("START_WITH", Operator.GetOperator(Operator.OperatorEnum.START_WITH));
            Assert.AreEqual("END_WITH", Operator.GetOperator(Operator.OperatorEnum.END_WITH));
            Assert.AreEqual("BETWEEN", Operator.GetOperator(Operator.OperatorEnum.BETWEEN));
            Assert.AreEqual("NOT_BETWEEN", Operator.GetOperator(Operator.OperatorEnum.NOT_BETWEEN));
            Assert.AreEqual("ISNULL", Operator.GetOperator(Operator.OperatorEnum.ISNULL));
            Assert.AreEqual("ISNOTNULL", Operator.GetOperator(Operator.OperatorEnum.ISNOTNULL));
        }

        [TestMethod]
        public void GetExpresionTest()
        {
            // Se comprueba la expresión asociada a cada operador texto resumido.
            Assert.AreEqual("", Operator.GetExpression(Operator.OperatorEnum.NOTHING));
            Assert.AreEqual("{f} = ?", Operator.GetExpression(Operator.OperatorEnum.EQUAL));
            Assert.AreEqual("{f} <> ?", Operator.GetExpression(Operator.OperatorEnum.NOTEQUAL));
            Assert.AreEqual("{f} > ?", Operator.GetExpression(Operator.OperatorEnum.GREATERTHAN));
            Assert.AreEqual("{f} < ?", Operator.GetExpression(Operator.OperatorEnum.LESSTHAN));
            Assert.AreEqual("{f} >= ?", Operator.GetExpression(Operator.OperatorEnum.GREATERTHAN_OR_EQUALTO));
            Assert.AreEqual("{f} <= ?", Operator.GetExpression(Operator.OperatorEnum.LESSTHAN_OR_EQUALTO));
            Assert.AreEqual("{f} IN ({v})", Operator.GetExpression(Operator.OperatorEnum.IN));
            Assert.AreEqual("{f} IN ({l})", Operator.GetExpression(Operator.OperatorEnum.LIST));
            Assert.AreEqual("{f} NOT IN ({l})", Operator.GetExpression(Operator.OperatorEnum.NOT_LIST));
            Assert.AreEqual("{f}.Contains(?)", Operator.GetExpression(Operator.OperatorEnum.LIKE));
            Assert.AreEqual("{f}.StartsWith(?)", Operator.GetExpression(Operator.OperatorEnum.START_WITH));
            Assert.AreEqual("{f}.EndsWith(?)", Operator.GetExpression(Operator.OperatorEnum.END_WITH));
            Assert.AreEqual("{f} BETWEEN ?0 AND ?1", Operator.GetExpression(Operator.OperatorEnum.BETWEEN));
            Assert.AreEqual("{f} NOT BETWEEN ?0 AND ?1", Operator.GetExpression(Operator.OperatorEnum.NOT_BETWEEN));
            Assert.AreEqual("{f} IS NULL", Operator.GetExpression(Operator.OperatorEnum.ISNULL));
            Assert.AreEqual("NOT {f} IS NULL", Operator.GetExpression(Operator.OperatorEnum.ISNOTNULL));
        }

        [TestMethod]
        public void GetOperatorEnumTest()
        {
            // Se comprueba el texto resumido por cada enum.
            Assert.AreEqual(Operator.OperatorEnum.NOTHING, Operator.GetOperatorEnum(""));
            Assert.AreEqual(Operator.OperatorEnum.EQUAL, Operator.GetOperatorEnum("="));
            Assert.AreEqual(Operator.OperatorEnum.NOTEQUAL, Operator.GetOperatorEnum("<>"));
            Assert.AreEqual(Operator.OperatorEnum.GREATERTHAN, Operator.GetOperatorEnum(">"));
            Assert.AreEqual(Operator.OperatorEnum.LESSTHAN, Operator.GetOperatorEnum("<"));
            Assert.AreEqual(Operator.OperatorEnum.GREATERTHAN_OR_EQUALTO, Operator.GetOperatorEnum(">="));
            Assert.AreEqual(Operator.OperatorEnum.LESSTHAN_OR_EQUALTO, Operator.GetOperatorEnum("<="));
            Assert.AreEqual(Operator.OperatorEnum.IN, Operator.GetOperatorEnum("IN"));
            Assert.AreEqual(Operator.OperatorEnum.LIST, Operator.GetOperatorEnum("LIST"));
            Assert.AreEqual(Operator.OperatorEnum.NOT_LIST, Operator.GetOperatorEnum("NOT_LIST"));
            Assert.AreEqual(Operator.OperatorEnum.LIKE, Operator.GetOperatorEnum("LIKE"));
            Assert.AreEqual(Operator.OperatorEnum.START_WITH, Operator.GetOperatorEnum("START_WITH"));
            Assert.AreEqual(Operator.OperatorEnum.END_WITH, Operator.GetOperatorEnum("END_WITH"));
            Assert.AreEqual(Operator.OperatorEnum.BETWEEN, Operator.GetOperatorEnum("BETWEEN"));
            Assert.AreEqual(Operator.OperatorEnum.NOT_BETWEEN, Operator.GetOperatorEnum("NOT_BETWEEN"));
            Assert.AreEqual(Operator.OperatorEnum.ISNULL, Operator.GetOperatorEnum("ISNULL"));
            Assert.AreEqual(Operator.OperatorEnum.ISNOTNULL, Operator.GetOperatorEnum("ISNOTNULL"));
        }

    }
}
