﻿using ArenaSoftware.ArenaCore.Security;
using ArenaSoftware.ArenaCore.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Principal;
using System.Threading;
using static ArenaSoftware.ArenaCore.Services.ActionWorkWith;

namespace ArenaCoreUnitTest
{
    [TestClass]
    public class PermissionCheckerTest
    {
        private IPermissionChecker checker;

        public PermissionCheckerTest()
        {
            var server = new TestHost().Server;
            checker = (IPermissionChecker) server.Host.Services.GetService(typeof(IPermissionChecker));
        }

        [TestMethod]
        public void TestNoAdminAccess()
        {
            // Autentica con "user".
            BasicAuthenticationIdentity identity = new BasicAuthenticationIdentity("user", "contraseña", "", "");
            var genericPrincipal = new GenericPrincipal(identity, null);
            Thread.CurrentPrincipal = genericPrincipal;

            // Para "testitem1" debería tener acceso total.
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem1"));

            // Para "testitem2" debería tener acceso parcial, pero "true" a todo el ítem en general.
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem2"));

            // Para "testitem3" debería tener acceso denegado.
            Assert.AreEqual(false, checker.CheckPermission("user", "testitem3"));

            // Para "testitem1" debería tener acceso total sobre la acción "I".
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem1", ActionWorkWithEnum.INSERT));
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem1", ActionWorkWith.InsertAccessCode));

            // Para "testitem2" NO debería tener acceso sobre la acción "I".
            Assert.AreEqual(false, checker.CheckPermission("user", "testitem2", ActionWorkWithEnum.INSERT));
            Assert.AreEqual(false, checker.CheckPermission("user", "testitem2", ActionWorkWith.InsertAccessCode));

            // Para "testitem2" debería tener acceso sobre la acción "V".
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem2", ActionWorkWithEnum.GET));
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem2", ActionWorkWith.GetAccessCode));

            // Para "testitem3" no hay acciones definidas, entonces cualquier pedido sobre aualquier acción debería ser falso.
            Assert.AreEqual(false, checker.CheckPermission("user", "testitem3", ActionWorkWithEnum.GET));
        }

        [TestMethod]
        public void TestAdminAccess()
        {
            // Autentica con "admin".
            BasicAuthenticationIdentity identity = new BasicAuthenticationIdentity("admin", "contraseña", "", "");
            var genericPrincipal = new GenericPrincipal(identity, null);
            Thread.CurrentPrincipal = genericPrincipal;

            // Debería tener acceso a todo lo que se le solicita ya que es "admin".
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem1"));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem2"));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem3"));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem1", ActionWorkWithEnum.INSERT));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem1", ActionWorkWith.InsertAccessCode));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem2", ActionWorkWithEnum.INSERT));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem2", ActionWorkWith.InsertAccessCode));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem2", ActionWorkWithEnum.GET));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem2", ActionWorkWith.GetAccessCode));
            Assert.AreEqual(true, checker.CheckPermission("admin", "testitem3", ActionWorkWithEnum.GET));
        }

        [TestMethod]
        public void TestClearCache()
        {
            // Autentica con "user".
            BasicAuthenticationIdentity identity = new BasicAuthenticationIdentity("user", "contraseña", "", "");
            var genericPrincipal = new GenericPrincipal(identity, null);
            Thread.CurrentPrincipal = genericPrincipal;

            // Pide revisar un permiso (acción que guarda en cache).
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem1"));

            // Limpia el cache.
            checker.ClearUserCache("user");

            // Se vuelve a pedir un permiso (acción que guarda en cache nuevamente).
            Assert.AreEqual(true, checker.CheckPermission("user", "testitem1"));
        }
    }
}
