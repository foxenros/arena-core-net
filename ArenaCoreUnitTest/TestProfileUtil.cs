﻿using ArenaSoftware.ArenaCore.Security;
using ArenaSoftware.ArenaCore.Services;
using System.Collections.Generic;

namespace ArenaCoreUnitTest
{
    public class TestProfileUtil : IProfileUtil
    {
        public Dictionary<string, ProfileItem> GetProfile(string userName)
        {
            Dictionary<string, ProfileItem> profile = new Dictionary<string, ProfileItem>();

            // Ítem de acceso completo.
            ProfileItem profileItem1 = new ProfileItem();
            profileItem1.MenuItId = "testitem1";
            profileItem1.Enabled = true;
            profileItem1.ProfileItemAccess.Add(ActionWorkWith.InsertAccessCode, new ProfileItemAccess(ActionWorkWith.InsertAccessCode, true));
            profileItem1.ProfileItemAccess.Add(ActionWorkWith.UpdateAccessCode, new ProfileItemAccess(ActionWorkWith.UpdateAccessCode, true));
            profileItem1.ProfileItemAccess.Add(ActionWorkWith.GetAccessCode, new ProfileItemAccess(ActionWorkWith.GetAccessCode, true));
            profileItem1.ProfileItemAccess.Add(ActionWorkWith.DeleteAccessCode, new ProfileItemAccess(ActionWorkWith.DeleteAccessCode, true));
            profile.Add("testitem1", profileItem1);

            // Ítem de acceso parcial.
            ProfileItem profileItem2 = new ProfileItem();
            profileItem2.MenuItId = "testitem2";
            profileItem2.Enabled = true;
            profileItem2.ProfileItemAccess.Add(ActionWorkWith.InsertAccessCode, new ProfileItemAccess(ActionWorkWith.InsertAccessCode, false));
            profileItem2.ProfileItemAccess.Add(ActionWorkWith.UpdateAccessCode, new ProfileItemAccess(ActionWorkWith.UpdateAccessCode, false));
            profileItem2.ProfileItemAccess.Add(ActionWorkWith.GetAccessCode, new ProfileItemAccess(ActionWorkWith.GetAccessCode, true));
            profileItem2.ProfileItemAccess.Add(ActionWorkWith.DeleteAccessCode, new ProfileItemAccess(ActionWorkWith.DeleteAccessCode, false));
            profile.Add("testitem2", profileItem2);

            // Ítem sin acceso.
            ProfileItem profileItem3 = new ProfileItem();
            profileItem3.MenuItId = "testitem3";
            profileItem3.Enabled = false;
            profile.Add("testitem3", profileItem3);

            return profile;
        }

        public bool IsAdmin(string userName)
        {
            return (userName == "admin");
        }
    }
}
