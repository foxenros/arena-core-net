﻿using ArenaSoftware.ArenaCore.Filter;
using ArenaSoftware.ArenaCore.Service;
using ArenaSoftware.ArenaCore.Validation;
using Microsoft.Extensions.Logging;

namespace ArenaCoreUnitTest
{
    [ArenaWorkWithService(keyField: "Id")]
    public class TestService : ArenaWorkWithService<TestEntity, int>
    {
        public TestService(TestDbContext context, ArenaValidationUtil arenaValidationUtil, FilterBuilder filterBuilder, ILogger<TestService> logger) : base(context, arenaValidationUtil, filterBuilder, logger)
        {
        }

    }
}
