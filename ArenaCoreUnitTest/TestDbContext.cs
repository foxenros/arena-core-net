﻿using Microsoft.EntityFrameworkCore;

namespace ArenaCoreUnitTest
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {
        }

        public DbSet<TestEntity> Test { get; set; }
    }
}
